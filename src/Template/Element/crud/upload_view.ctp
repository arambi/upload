<div class="upload-view">
  <div class="upload-view-image">
    <h2>{{ data.content.filename }}</h2>
    <img ng-src="{{ data.content.paths.org }}" alt="">
  </div>
  <div class="upload-view-info">
    <h3>Enlaces</h3>
    <div ng-repeat="link in data.links" class="upload-view-info-link">
      <div class="_link" ng-if="link.front">
        <div class="_link-title"><a target="_blank" href="{{ link.front }}">Web</a></div>
        <div class="_link-url"><a target="_blank" href="{{ link.front }}">{{ link.front }}</a></div>
      </div>
      <div class="_link">
        <div class="_link-title"><a cf-modal="link.admin.url" size="big" ng-model="content">Admin - {{ link.admin.title }}</a></div>
      </div>
    </div>
  </div>
</div>