<?php

namespace Upload\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\ORM\Table;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\Event\Event;
use Cake\Filesystem\Folder;
use Cake\Utility\Inflector;
use ArrayObject;
use Cake\Core\Configure;
use Cake\Log\Log;
use Cake\Utility\Text;
use Upload\Utils\WideImage;
use Gumlet\ImageResize;

/**
 * Upload behavior
 */
class UploadBehavior extends Behavior
{

  const MOBILE_WIDTH = 820;

  /**
   * Default configuration.
   *
   * @var array
   */
  public $defaults = [
    'rootDir'     => null,
    'pathMethod'    => 'random',
    'path'        => '{WWW_ROOT}files{DS}',
    'fields'      => [
      'dir' => 'path',
      'type' => 'mimetype',
      'size' => 'filesize',
      'original_filename' => 'original_filename'
    ],
    'mimetypes'     => array(),
    'extensions'    => array(),
    'maxSize'     => 2097152,
    'minSize'     => 8,
    'maxHeight'     => 0,
    'minHeight'     => 0,
    'maxWidth'      => 0,
    'minWidth'      => 0,
    'thumbnails'    => true,
    'thumbnailMethod' => 'php',
    'thumbnailName'   => null,
    'thumbnailPath'   => null,
    'thumbnailPrefixStyle' => true,
    'thumbnailQuality'  => 99,
    'thumbnailSizes'  => array(),
    'thumbnailType'   => false,
    'deleteOnUpdate'  => false,
    'mediaThumbnailType' => 'png',
    'saveDir'     => true,
  ];

  protected $_imageMimetypes = array(
    'image/bmp',
    'image/gif',
    'image/jpeg',
    'image/pjpeg',
    'image/png',
    'image/vnd.microsoft.icon',
    'image/x-icon',
    'application/octet-stream',
  );

  protected $_mediaMimetypes = array(
    'application/pdf',
    'application/postscript',
  );

  protected $_videoMimetypes = array(
    // 'video/mpeg',
    // 'video/mp4',
    // 'video/ogg',
    // 'video/quicktime',
    // 'video/webm',
    // 'video/x-matroska',
    // 'video/x-ms-wmv',
    // 'video/x-msvideo',
    // 'video/x-flv'
  );

  protected $_pathMethods = array('flat', 'primaryKey', 'random');

  protected $_resizeMethods = array('imagick', 'php');

  private $__filesToRemove = array();

  protected $_removingOnly = array();

  /**
   * Runtime configuration for this behavior
   *
   * @var array
   **/
  public $runtime;

  public function initialize(array $config)
  {
    parent::initialize($config);

    $this->defaults['path'] = '{WWW_ROOT}' . Configure::read('App.filesBaseUrl');

    foreach ($config as $field => $options) {
      $this->_setupField($field, $options);
    }
  }

  public function uploadConfig()
  {
    return $this->config();
  }

  public function _setupField($field, $options)
  {
    if (is_int($field)) {
      $field = $options;
      $options = array();
    }

    $this->defaults['rootDir'] = '';

    $options = array_merge($this->defaults, (array) $options);

    if (isset($options['thumbnailSizes'])) {
      foreach ($options['thumbnailSizes'] as $thm => $size) {
        if (is_array($size)) {
          $options['thumbnailSizes'][$thm] = $size['size'];
        }
      }
    }
    $options['fields'] += $this->defaults['fields'];
    if ($options['rootDir'] === null) {
      $options['rootDir'] = $this->defaults['rootDir'];
    }

    if ($options['thumbnailName'] === null) {
      if ($options['thumbnailPrefixStyle']) {
        $options['thumbnailName'] = '{size}_{filename}';
      } else {
        $options['thumbnailName'] = '{filename}_{size}';
      }
    }

    if ($options['thumbnailPath'] === null) {
      $options['thumbnailPath'] = Folder::slashTerm($this->_path($field, array(
        'isThumbnail' => true,
        'path' => $options['path'],
        'rootDir' => $options['rootDir']
      )));
    } else {
      $options['thumbnailPath'] = Folder::slashTerm($this->_path($field, array(
        'isThumbnail' => true,
        'path' => $options['thumbnailPath'],
        'rootDir' => $options['rootDir']
      )));
    }

    $options['path'] = Folder::slashTerm($this->_path($field, array(
      'isThumbnail' => false,
      'path' => $options['path'],
      'rootDir' => $options['rootDir']
    )));

    if (!in_array($options['thumbnailMethod'], $this->_resizeMethods)) {
      $options['thumbnailMethod'] = 'imagick';
    }
    if (!in_array($options['pathMethod'], $this->_pathMethods)) {
      $options['pathMethod'] = 'primaryKey';
    }
    $options['pathMethod'] = '_getPath' . Inflector::camelize($options['pathMethod']);
    $options['thumbnailMethod'] = 'resize' . Inflector::camelize($options['thumbnailMethod']);
    $this->config($field, $options);
  }


  public function beforeMarshal(Event $event, ArrayObject $data)
  {
    foreach ($this->config() as $field => $options) {
      if (isset($data[$field]) && is_array($data[$field])) {
        $data[$field] = json_encode($data[$field]);
      }
    }
  }

  /**
   * Before save method. Called before all saves
   *
   * Handles setup of file uploads
   *
   * @param AppModel $model Model instance
   * @return boolean
   */
  public function beforeSave(Event $event, Entity $entity, ArrayObject $_options)
  {
    $this->_removingOnly = array();

    foreach ($this->config() as $field => $options) {
      if (!isset($entity->$field)) {
        continue;
      }
      $this->runtime[$field] = json_decode($entity->$field, true);

      if (!$this->validateTypeFile($entity, $field)) {
        $entity->errors('filename', __d('admin', 'El formato del fichero no es correcto'));
        return false;
      }

      if (!$this->validateSize($entity, $field)) {
        $entity->errors('filename', __d('admin', 'El tamaño del fichero no es correcto'));
        return false;
      }



      $date = array_key_exists('date', $_options) ? strftime($_options['date']) : time();
      $tempPath = $this->_getPath($entity, $field, $date);

      if (!$tempPath) {
        $entity->errors('filename', __d('admin', 'El directorio no tiene permisos'));
        return false;
      }

      $entity->set('path', $tempPath);

      $removing = array_key_exists('remove', $this->runtime[$field]);

      if ($removing || ($this->config($field . '.deleteOnUpdate')
        && array_key_exists('name', $this->runtime[$field])
        && strlen($this->runtime[$field]['name']))) {
        // We're updating the file, remove old versions
        if (!empty($entity->id)) {
          $data = $this->_table->find('all', array(
            'conditions' => array($this->_table->primaryKey() => $entity->id),
            'contain' => false,
            'recursive' => -1,
          ))->first();
          $this->_prepareFilesForDeletion($field, $data, $options);
        }

        if ($removing) {
          $entity->set([
            $field => null,
            $options['fields']['type'] => null,
            $options['fields']['size'] => null,
            $options['fields']['dir'] => null,
          ]);

          $this->_removingOnly[$field] = true;
          continue;
        } else {
          $entity->set([
            $field => null,
            $options['fields']['type'] => null,
            $options['fields']['size'] => null,
          ]);
        }
      } elseif (!array_key_exists('name', $this->runtime[$field])) {
        // if field is empty, don't delete/nullify existing file
        unset($this->runtime[$field]);
        continue;
      }

      $filename = $this->getFilename($field);
      $entity->set([
        'filename' => $filename,
        'extension' =>  pathinfo($filename, PATHINFO_EXTENSION),
        $options['fields']['type'] => $this->runtime[$field]['type'],
        $options['fields']['size'] => $this->runtime[$field]['size'],
        $options['fields']['original_filename'] => $this->runtime[$field]['name'],
        'mimetype' => $this->runtime[$field]['type'],
      ]);
    }
  }

  public function afterSave(Event $event, Entity $entity)
  {
    $temp = [];

    foreach ($this->config() as $field => $options) {
      if (!$entity->has($field)) {
        continue;
      }

      if (empty($this->runtime[$field])) {
        continue;
      }

      if (isset($this->_removingOnly[$field])) {
        continue;
      }

      $path = $this->config($field . '.path');
      $thumbnailPath = $this->config($field . '.thumbnailPath');

      if (!empty($entity->path)) {
        $path .= $entity->path . DS;
        $thumbnailPath .= $entity->path . DS;
      }

      $tmp = $this->runtime[$field]['tmp_name'];
      $filename = $entity->filename;
      $filePath = $path . $filename;

      if (!$this->handleUploadedFile($this->_table->alias(), $field, $tmp, $filePath)) {
        $entity->errors($field, __d('admin', 'No ha sido posible subir el fichero'));
        return false;
      }

      $this->_createThumbnails($entity, $field, $path, $thumbnailPath);
      $errors = $this->_table->validator()->errors($entity->toArray());

      if (!empty($errors)) {
        throw new \RuntimeException($errors[$field][0]);
      }

      if ($this->_table->hasField($options['fields']['dir'])) {
        if ($entity->isNew() && $options['pathMethod'] == '_getPathFlat') {
        } else {
          $pathinfo = pathinfo($filePath);
          $upload = $this->_table->newEntity([
            $options['fields']['dir'] => $entity->path,
            'extension' => $pathinfo['extension']
          ]);

          $upload->set('id', $entity->id);
          $this->_table->save($upload);
        }
      }
    }

    if (empty($this->__filesToRemove[$this->_table->alias()])) {
      return true;
    }

    foreach ($this->__filesToRemove[$this->_table->alias()] as $file) {
      $result[] = $this->unlink($file);
    }
    return $result;
  }

  private function getFilename($field)
  {
    $name = $this->runtime[$field]['name'];

    $ext = pathinfo($name, PATHINFO_EXTENSION);
    $filename = strtolower(Text::slug(pathinfo($name, PATHINFO_FILENAME))) . '.' . $ext;
    return $this->getUniqueName($filename);
  }

  private function getUniqueName($name, $sufix = 0)
  {
    if ($sufix > 0) {
      $filename = pathinfo($name, PATHINFO_FILENAME);
      $ext = pathinfo($name, PATHINFO_EXTENSION);
      $name_to_search = $filename . '-' . $sufix . '.' . $ext;
    } else {
      $name_to_search = $name;
    }

    if (!$this->_table->exists([
      'filename' => $name_to_search
    ])) {
      return $name_to_search;
    }

    return $this->getUniqueName($name, ($sufix + 1));
  }

  public function restoreThumbails($entity, $field, $change_type = false)
  {
    $path = $this->config($field . '.path') . $entity->path . '/';
    $this->runtime[$field]['type'] = $entity->mimetype;
    $this->_createThumbnails($entity, $field, $path, $path, $change_type);
  }

  public function validateSize(Entity $entity, $field)
  {
    if (isset($entity->content_type)) {
      $config = $this->getUploadConfig($entity->content_type);

      if ($config && isset($config['type']) && $config['type'] == 'image') {
        $data = $this->runtime[$field];
        $size = filesize($data['tmp_name']);
      }
    }
    $data = $this->runtime[$field];

    if (empty(filesize($data['tmp_name']))) {
      return false;
    }

    return true;
  }

  public function getUploadConfig($content_type)
  {
    return Configure::read('Upload.' . $content_type);
  }

  public function validateTypeFile(Entity $entity, $field)
  {
    if (isset($entity->content_type)) {
      $config = $this->getUploadConfig($entity->content_type);

      if ($config && isset($config['type']) && $config['type'] == 'image') {
        $data = $this->runtime[$field];

        $info = @getimagesize($data['tmp_name']);

        if (!$info || !in_array($info['mime'], $this->_imageMimetypes)) {
          $entity->errors('filename', __d('admin', 'El formato de imagen no es correcto'));
          return false;
        }
      }

      if ($config && isset($config['type']) && $config['type'] == 'video') {
        if (!in_array($data['type'], $this->_videoMimetypes)) {
          $entity->errors('filename', __d('admin', 'El formato de video no es correcto'));
          return false;
        }
      }
    }

    return true;
  }

  public function handleUploadedFile($modelAlias, $field, $tmp, $filePath)
  {
    if (is_uploaded_file($tmp)) {
      return @move_uploaded_file($tmp, $filePath);
    } else {
      return @rename($tmp, $filePath);
    }
    // return is_uploaded_file($tmp) && @move_uploaded_file($tmp, $filePath);
  }

  public function unlink($file)
  {
    return @unlink($file);
  }

  public function beforeDelete(Model $model, $cascade = true)
  {
    $data = $model->find('first', array(
      'conditions' => array("{$model->alias}.{$model->primaryKey}" => $model->id),
      'contain' => false,
      'recursive' => -1,
    ));

    foreach ($this->settings[$model->alias] as $field => $options) {
      $this->_prepareFilesForDeletion($model, $field, $data, $options);
    }
    return true;
  }

  public function afterDelete(Model $model)
  {
    $result = array();
    if (!empty($this->__filesToRemove[$model->alias])) {
      foreach ($this->__filesToRemove[$model->alias] as $file) {
        $result[] = $this->unlink($file);
      }
    }
    return $result;
  }

  public function getDimensions($src, $geometry)
  {
    $srcW = imagesx($src);
    $srcH = imagesy($src);

    // determine destination dimensions and resize mode from provided geometry
    if (preg_match('/^\\[[\\d]+x[\\d]+\\]$/', $geometry)) {
      // resize with banding
      list($W, $H) = explode('x', substr($geometry, 1, strlen($geometry) - 2));

      if ($srcW > $srcH) // Apaisada
      {
        $destW = $W;
        $resizeMode = false;
      } else // Alta
      {
        $destH = $H;
        $resizeMode = false;
      }
    } elseif (preg_match('/^[\\d]+x[\\d]+$/', $geometry)) {
      // cropped resize (best fit)
      list($destW, $destH) = explode('x', $geometry);
      $resizeMode = 'best';
    } elseif (preg_match('/^[\\d]+w$/', $geometry)) {
      // calculate heigh according to aspect ratio
      $destW = (int)$geometry;
      $resizeMode = false;
    } elseif (preg_match('/^[\\d]+h$/', $geometry)) {
      // calculate width according to aspect ratio
      $destH = (int)$geometry;
      $resizeMode = false;
    } elseif (preg_match('/^[\\d]+l$/', $geometry)) {
      // calculate shortest side according to aspect ratio
      if ($srcW > $srcH) {
        $destW = (int)$geometry - 1;
      } else {
        $destH = (int)$geometry - 1;
      }
      $resizeMode = false;
    }


    // _d( $destH);

    if (!isset($destW)) {
      $destW = ($destH / $srcH) * $srcW;
    }
    if (!isset($destH)) {
      $destH = ($destW / $srcW) * $srcH;
    }

    // determine resize dimensions from appropriate resize mode and ratio
    if ($resizeMode == 'best') {
      // "best fit" mode
      if ($srcW > $srcH) {
        if ($srcH / $destH > $srcW / $destW) {
          $ratio = $destW / $srcW;
        } else {
          $ratio = $destH / $srcH;
        }
      } else {
        if ($srcH / $destH < $srcW / $destW) {
          $ratio = $destH / $srcH;
        } else {
          $ratio = $destW / $srcW;
        }
      }

      $resizeW = $srcW * $ratio;
      $resizeH = $srcH * $ratio;
    } else if ($resizeMode == 'band') {
      // "banding" mode
      if ($srcW > $srcH) {
        $ratio = $destW / $srcW;
      } else {
        $ratio = $destH / $srcH;
      }

      $resizeW = $srcW * $ratio;
      $resizeH = $srcH * $ratio;
    } else {
      // no resize ratio
      $resizeW = $destW;
      $resizeH = $destH;
    }

    return compact('srcW', 'srcH', 'resizeH', 'resizeW', 'destW', 'destH');
  }

  public function _getPath(Entity $entity, $field, $date)
  {
    if (!empty($entity->path)) {
      return $entity->path;
    }

    $path = $this->config($field . '.path');
    $pathMethod = $this->config($field . '.pathMethod');

    if (method_exists($this, $pathMethod)) {
      return $this->$pathMethod($entity, $field, $path, $date);
    }

    return $this->_getPathPrimaryKey($entity, $field, $path);
  }

  // public function getFilename( Entity $entity, $field)
  // {
  //   $filename = $this->runtime [$field]['name'];
  //   return $this->_doFilename( $filename, $entity);
  // }

  protected function _doFilename($filename, $entity, $sufix = 0)
  {
    if ($this->_table->exists([
      'filename' => $this->_appendSufix($filename, $sufix),
      'path' => $entity->path
    ])) {
      $sufix++;
      return $this->_doFilename($filename, $entity, $sufix);
    }

    return $this->_appendSufix($filename, $sufix);
  }

  protected function _appendSufix($filename, $sufix)
  {
    if ($sufix > 0) {
      $name = substr($filename, 0, strrpos($filename, '.'));
      $ext = substr($filename, strrpos($filename, '.'));
      $filename = $name . '-' . $sufix . $ext;
    }

    return $filename;
  }

  public function _getPathFlat(Entity $entity, $field, $path)
  {
    $destDir = $path;
    $this->_mkPath($destDir);
    return '';
  }

  public function _getPathPrimaryKey(Entity $entity, $field, $path)
  {
    $destDir = $path . $entity->id . DIRECTORY_SEPARATOR;
    $this->_mkPath($destDir);
    return $model->id;
  }

  public function _getPathRandom(Entity $entity, $field, $path, $date = null)
  {
    $permissions =  substr(sprintf('%o', @fileperms($path)), -4);

    if ($date === null) {
      $date = time();
    }

    // if( $permissions != '0777')
    // {
    //   return false;
    // }

    $endPath = null;
    $decrement = 0;
    $string = crc32($field . time());

    for ($i = 0; $i < 3; $i++) {
      $decrement = $decrement - 2;
      $endPath .= sprintf("%02d" . DIRECTORY_SEPARATOR, substr('000000' . $string, $decrement, 2));
    }

    $endPath = date('Y', $date) . DIRECTORY_SEPARATOR . date('m', $date) . DIRECTORY_SEPARATOR;
    $destDir = $path . $endPath . DIRECTORY_SEPARATOR;
    // $destDir = $path . $endPath;

    if (!$this->_mkPath($destDir)) {
      return false;
    }

    return substr($endPath, 0, -1);
  }

  public function _mkPath($request)
  {
    if (!$request) return;
    $paths = explode('/', substr($request, 0, -1));
    $paths = array_slice($paths, count($paths) - 3);
    $server_path = str_replace(implode('/', $paths) . '/', '', $request);
    $route = $paths[0];

    foreach ($paths as $key => $path) {
      if ($key > 0)
        $route .= "/$path";

      $Folder = new Folder;

      if (!$Folder->create($server_path . $route, 0777)) {
        return false;
      } else {
        @chmod($path, intval('0777', 8));
        // $Folder->chmod( $server_path . $route, '0777');
      }
    }
    return true;
  }

  /**
   * Returns a path based on settings configuration
   *
   * @return string
   **/
  public function _path($fieldName, $options = array())
  {
    $defaults = array(
      'isThumbnail' => true,
      'path' => '{ROOT}webroot{DS}files{DS}{model}{DS}{field}{DS}',
      'rootDir' => $this->defaults['rootDir'],
    );

    $options = array_merge($defaults, $options);

    foreach ($options as $key => $value) {
      if ($value === null) {
        $options[$key] = $defaults[$key];
      }
    }

    if (!$options['isThumbnail']) {
      $options['path'] = str_replace(array('{size}', '{geometry}'), '', $options['path']);
    }

    $replacements = array(
      '{WWW_ROOT}' => WWW_ROOT,
      '{ROOT}'  => $options['rootDir'],
      '{model}' => Inflector::underscore($this->_table->alias()),
      '{field}' => $fieldName,
      '{DS}'    => DIRECTORY_SEPARATOR,
      '//'    => DIRECTORY_SEPARATOR,
      '/'     => DIRECTORY_SEPARATOR,
      '\\'    => DIRECTORY_SEPARATOR,
    );

    $newPath = Folder::slashTerm(str_replace(
      array_keys($replacements),
      array_values($replacements),
      $options['path']
    ));

    if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
      if (!preg_match('/^([a-zA-Z]:\\\|\\\\)/', $newPath)) {
        $newPath = $options['rootDir'] . $newPath;
      }
    } elseif ($newPath[0] !== DIRECTORY_SEPARATOR) {
      $newPath = $options['rootDir'] . $newPath;
    }

    $pastPath = $newPath;
    while (true) {
      $pastPath = $newPath;
      $newPath = str_replace(array(
        '//',
        '\\',
        DIRECTORY_SEPARATOR . DIRECTORY_SEPARATOR
      ), DIRECTORY_SEPARATOR, $newPath);
      if ($pastPath == $newPath) {
        break;
      }
    }

    return $newPath;
  }

  public function _pathThumbnail($field, $params = array())
  {
    return str_replace(
      array('{size}', '{geometry}'),
      array($params['size'], $params['geometry']),
      $params['thumbnailPath']
    );
  }

  public function _createThumbnails(Entity $entity, $field, $path, $thumbnailPath, $change_type = false)
  {
    $isImage = $this->_isImage($this->runtime[$field]['type']);
    $isMedia = $this->_isMedia($this->runtime[$field]['type']);
    $hasThumbnails = !(!$this->config($field . '.thumbnailSizes'));

    if (($isImage || $isMedia) && $hasThumbnails) {
      WideImage::adjustOrientation($path . $entity->$field);

      foreach ($this->config($field . '.thumbnailSizes') as $size => $geometry) {
        if (is_array($geometry)) {
          $geometry = $geometry['size'];
        }

        $thumbnailPathSized = $this->_pathThumbnail($field, compact(
          'geometry',
          'size',
          'thumbnailPath'
        ));

        if (!$this->resizePhp($entity, $field, $path, $size, $geometry, $thumbnailPathSized, $change_type)) {
          // $model->invalidate($field, __d( "upload", "No ha podido procesarse el archivo"));
        }
      }
    }
  }

  public function resizePhp(Entity $entity, $field, $path, $size, $geometry, $thumbnailPath, $change_type)
  {
    $srcFile  = $path . $entity->$field;

    try {
      return $this->resizeThmPhp($entity, $srcFile, $field, $path, $size, $geometry, $thumbnailPath, false, $change_type);
    } catch (\Throwable $th) {
      return false;
    }
  }

  public function resizeThmPhp(Entity $entity, $srcFile, $field, $path, $size, $geometry, $thumbnailPath, $filename_force = false, $change_type = false)
  {
    $pathInfo = $this->_pathinfo($srcFile);
    $thumbnailType = $pathInfo['extension'];

    if (!$thumbnailType) {
      $thumbnailType = 'png';
    }

    if ($filename_force) {
      $fileName = str_replace(
        array('{size}', '{filename}', '{primaryKey}'),
        array($size, $filename_force, $entity->id),
        $this->config($field . '.thumbnailName')
      );

      $destFile = "{$thumbnailPath}{$fileName}";
    } else {
      $fileName = str_replace(
        ['{size}', '{filename}', '{primaryKey}'],
        [$size, $pathInfo['filename'], $entity->id],
        $this->config($field . '.thumbnailName')
      );

      if ($change_type) {
        $fileName .= '_' . $entity->content_type;
      }

      $destFile = "{$thumbnailPath}{$fileName}.{$thumbnailType}";
    }

    $quality = $this->config($field . '.thumbnailQuality');
    $image = new ImageResize($srcFile);

    if ($thumbnailType == 'png') {
      $image->quality_png = round($quality / 10);

      if ($image->quality_png > 9) {
        $image->quality_png = 9;
      }
    } else {
      $image->quality_jpg = $quality;
    }

    if (strpos($geometry, 'w') !== false) {
      $width = (int)str_replace('w', '', $geometry);

      $image->resizeToWidth($width);
      $image->save($destFile);

      if ($image->getDestWidth() > self::MOBILE_WIDTH) {
        $image = new ImageResize($srcFile);
        $image->resizeToWidth(self::MOBILE_WIDTH);
        $image->save(str_replace($fileName, 'mobile_' . $fileName, $destFile));
      }

      return true;
    } elseif (strpos($geometry, 'h') !== false) {
      $height = (int)str_replace('h', '', $geometry);
      $image->resizeToHeight($height);
      $image->save($destFile);

      if ($image->getDestWidth() > self::MOBILE_WIDTH) {
        $image = new ImageResize($srcFile);
        $image->resizeToWidth(self::MOBILE_WIDTH);
        $image->save(str_replace($fileName, 'mobile_' . $fileName, $destFile));
      }

      return true;
    } elseif (preg_match('/^[\\d]+x[\\d]+$/', $geometry)) {
      // resize with banding
      list($width, $height) = explode('x', $geometry);
      $image->crop((int)$width, (int)$height, true, ImageResize::CROPCENTER);
      $image->save($destFile);

      if ($image->getDestWidth() > self::MOBILE_WIDTH) {
        $image = new ImageResize($srcFile);
        $height = round(($height * self::MOBILE_WIDTH) / $width);
        $image->crop(self::MOBILE_WIDTH, $height, true, ImageResize::CROPCENTER);
        $image->save(str_replace($fileName, 'mobile_' . $fileName, $destFile));
      }

      return true;
    } else {
      return false;
    }
  }

  public function _isImage($mimetype)
  {
    return in_array($mimetype, $this->_imageMimetypes);
  }

  public function _isMedia($mimetype)
  {
    return in_array($mimetype, $this->_mediaMimetypes);
  }

  public function _isVideo($mimetype)
  {
    return in_array($mimetype, $this->_videoMimetypes);
  }

  public function _getMimeType($filePath)
  {
    if (class_exists('finfo')) {
      $finfo = new \finfo(defined('FILEINFO_MIME_TYPE') ? FILEINFO_MIME_TYPE : FILEINFO_MIME);
      return $finfo->file($filePath);
    }

    return mime_content_type($filePath);
  }


  public function _prepareFilesForDeletion(&$model, $field, $data, $options)
  {
    if (!strlen($data[$model->alias][$field])) return $this->__filesToRemove;

    $dir = $data[$model->alias][$options['fields']['dir']];
    $filePathDir = $this->settings[$model->alias][$field]['path'] . $dir . DS;
    $filePath = $filePathDir . $data[$model->alias][$field];
    $pathInfo = $this->_pathinfo($filePath);

    $this->__filesToRemove[$model->alias] = array();
    $this->__filesToRemove[$model->alias][] = $filePath;

    $hasThumbnails = !empty($options['thumbnailSizes']);

    if (!$hasThumbnails) {
      return $this->__filesToRemove;
    }

    $DS = DIRECTORY_SEPARATOR;
    $mimeType = $this->_getMimeType($filePath);
    $isMedia = $this->_isMedia($mimeType);
    $isImagickResize = $options['thumbnailMethod'] == 'imagick';
    $thumbnailType = $options['thumbnailType'];

    if ($isImagickResize) {
      if ($isMedia) {
        $thumbnailType = $options['mediaThumbnailType'];
      }

      if (!$thumbnailType || !is_string($thumbnailType)) {
        try {
          $srcFile = $filePath;
          $image    = new imagick();
          if ($isMedia) {
            $image->setResolution(300, 300);
            $srcFile = $srcFile . '[0]';
          }

          $image->readImage($srcFile);
          $thumbnailType = $image->getImageFormat();
        } catch (Exception $e) {
          $thumbnailType = 'png';
        }
      }
    } else {
      if (!$thumbnailType || !is_string($thumbnailType)) {
        $thumbnailType = $pathInfo['extension'];
      }

      if (!$thumbnailType) {
        $thumbnailType = 'png';
      }
    }

    foreach ($options['thumbnailSizes'] as $size => $geometry) {
      if (is_array($geometry)) {
        $geometry = $geometry['size'];
      }

      $fileName = str_replace(
        array('{size}', '{filename}', '{primaryKey}'),
        array($size, $pathInfo['filename'], $model->id),
        $options['thumbnailName']
      );

      $thumbnailPath = $options['thumbnailPath'];
      $thumbnailPath = $this->_pathThumbnail($field, compact(
        'geometry',
        'size',
        'thumbnailPath'
      ));

      $thumbnailFilePath = "{$thumbnailPath}{$dir}{$DS}{$fileName}.{$thumbnailType}";
      $this->__filesToRemove[$model->alias][] = $thumbnailFilePath;
    }
    return $this->__filesToRemove;
  }

  public function _pathinfo($filename)
  {
    $pathInfo = pathinfo($filename);

    if (!isset($pathInfo['extension']) || !strlen($pathInfo['extension'])) {
      $pathInfo['extension'] = '';
    }

    // PHP < 5.2.0 doesn't include 'filename' key in pathinfo. Let's try to fix this.
    if (empty($pathInfo['filename'])) {
      $pathInfo['filename'] = basename($pathInfo['basename'], '.' . $pathInfo['extension']);
    }
    return $pathInfo;
  }
}
