<?php

use Phinx\Migration\AbstractMigration;

class Upload extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
      $uploads = $this->table( 'uploads');
      $uploads->addColumn( 'content_type', 'string', ['limit' => 50])
              ->addColumn( 'filename', 'string', ['default' => NULL, 'null' => true])
              ->addColumn( 'path', 'string', ['limit' => 50, 'default' => NULL, 'null' => true])
              ->addColumn( 'original_filename', 'string', ['default' => NULL, 'null' => true])
              ->addColumn( 'filesize', 'integer', ['default' => NULL, 'null' => true])
              ->addColumn( 'extension', 'string', ['limit' => 10, 'default' => NULL, 'null' => true])
              ->addColumn( 'mimetype', 'string', ['limit' => 50, 'default' => NULL, 'null' => true])
              ->addColumn( 'duration', 'string', ['limit' => 20, 'default' => NULL, 'null' => true])
              ->addColumn( 'created', 'datetime', ['default' => null])
              ->addColumn( 'modified', 'datetime', ['default' => null])
              ->save();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
      $this->dropTable( 'uploads');
    }
}