<?php
namespace Upload\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Upload\Model\Table\UploadsTable;
use Cake\TestSuite\TestCase;	
use Cake\Core\Plugin;
use Cake\Core\Configure;
use Upload\Lib\UploadUtil;
use Cake\Filesystem\Folder;

/**
 * Upload\Model\Table\UploadsTable Test Case
 */
class UploadsTableTest extends TestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = [
		'plugin.upload.uploads'
	];

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$config = TableRegistry::exists('Uploads') ? [] : ['className' => 'Upload\Model\Table\UploadsTable'];
		$this->Uploads = TableRegistry::get('Uploads', $config);
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		$this->Uploads->removeBehavior( 'Upload');
		unset($this->Uploads);
		unset( $this->filepath);
		parent::tearDown();
	}

/**
 * Configuración del upload para que se pueda usar en varios test
 */
	public function configure()
	{
		$this->filepath = Plugin::path( 'Upload') .'tests' .DS. 'files'. DS;
		$filespath = TMP .'tests' .DS;
		Configure::write( 'App.filesBaseUrl', TMP .'tests' .DS);


		Configure::write( 'Upload', [
			'post' => [
				'type' => 'image',
	      'template' => 'image',
	      'thumbail' => 'med',
	      'config' => [
	      		'path' => TMP .'tests{DS}',
	          'thumbnailQuality' => 100,
	          'thumbnailSizes' => [
	              'thm' => [
	              	'size' => '200w',
	              	'name' => 'Miniatura',
	              ],
	              'hor' => [
	              	'size' => '400x300',
	              	'name' => 'Horizontal',
	              ],
	              'square' => [
	              	'size' => '400x400',
	              	'name' => 'Cuadrada'
	              ],
	              'big' => [
	              	'size' => '400h',
	              	'name' => 'Grande'
	              ]
	          ],
	      ]
			]
		]);

		$this->Uploads->addBehavior( 'Upload.Upload', [
			'filename' => Configure::read( 'Upload.post.config')
		]);
	}

	public function testConfig()
	{
		$this->configure();
		$config = $this->Uploads->uploadConfig();
	}


/**
 * Test para comprobar el correcto procesamiento de las imágenes
 */
	public function testSaveUploadPhotoSucess() 
	{
		$this->configure();

		$temp_file = $this->filepath . 'woman2.jpg';
		copy( $this->filepath . 'woman.jpg', $this->filepath . 'woman2.jpg');

		$upload = $this->Uploads->newEntity([
			'filename' => [
				'name' => 'test.jpg',
        'type' => 'image/jpeg',
        'size' => 542,
        'tmp_name' => $temp_file,
        'error' => 0
			],
			'content_type' => 'post'
		]);
		
		$saved = $this->Uploads->save( $upload);
		
		$new = $this->Uploads->findById( $upload->id)->first();

		$images = [];

		foreach( $new->paths as $key =>$path)
		{
			$images [$key] = getimagesize( substr( $path, 1));
			$this->assertEquals( 'image/jpeg', $images [$key]['mime']);
		}

		// Anchura para un cálculo de 200w
		$this->assertEquals( 200, $images ['thm'][0]);

		// Anchura para un cálculo de 400x300
		$this->assertEquals( 400, $images ['hor'][0]);

		// Altura para un cálculo de 400x300
		$this->assertEquals( 300, $images ['hor'][1]);

		// Anchura para un cálculo de 400x400
		$this->assertEquals( 400, $images ['square'][0]);

		// Altura para un cálculo de 400x400
		$this->assertEquals( 400, $images ['square'][1]);

		// Altura para un cálculo de 400h
		$this->assertEquals( 400, $images ['big'][1]);

		// Borra el directorio
		$Folder = new Folder();
		$Folder->delete( Configure::read( 'App.filesBaseUrl') . substr( $new->path, 0, 2));
	}


/**
 * Test para comprobar el procesamiento de una foto con error
 */
	public function testSaveUploadPhotoError() 
	{
		$this->configure();

		$temp_file = $this->filepath . 'error2.jpg';
		copy( $this->filepath . 'error.jpg', $this->filepath . 'error2.jpg');

		$upload = $this->Uploads->newEntity([
			'filename' => [
				'name' => 'test.jpg',
        'type' => 'image/jpeg',
        'size' => 542,
        'tmp_name' => $temp_file,
        'error' => 0
			],
			'content_type' => 'post'
		]);


		$saved = $this->Uploads->save( $upload);
		$errors = $upload->errors();
		// Comprueba que hay un error en el campo filename
		$this->assertTrue( array_key_exists( 'filename', $errors));

		// Comprueba que no se ha guardado el upload
		$new = $this->Uploads->findById( $upload->id)->first();
		$this->assertTrue( $new === null);
	}

	/**
 * Test para comprobar el procesamiento de una foto con error
 */
	public function testSaveUploadFolderError() 
	{
		$this->filepath = Plugin::path( 'Upload') .'tests' .DS. 'files'. DS;
		Configure::write( 'App.filesBaseUrl', TMP .'aaa' .DS);


		Configure::write( 'Upload', [
			'post' => [
				'type' => 'image',
	      'template' => 'image',
	      'thumbail' => 'med',
	      'config' => [
	      		'path' => TMP .'aaa{DS}',
	          'thumbnailMethod' => 'php',
	          'pathMethod' => 'random',
	          'thumbnailQuality' => 100,
	          'thumbnailSizes' => [
	              'thm' => '200w',
	              'hor' => '400x300',
	              'square' => '400x400',
	              'big' => '400h'
	          ],
	      ]
			]
		]);

		$this->Uploads->addBehavior( 'Upload.Upload', [
			'filename' => Configure::read( 'Upload.post.config')
		]);

		$temp_file = $this->filepath . 'woman2.jpg';
		copy( $this->filepath . 'woman.jpg', $this->filepath . 'woman2.jpg');

		$upload = $this->Uploads->newEntity([
			'filename' => [
				'name' => 'test.jpg',
        'type' => 'image/jpeg',
        'size' => 542,
        'tmp_name' => $temp_file,
        'error' => 0
			],
			'content_type' => 'post'
		]);


		$saved = $this->Uploads->save( $upload);
		$errors = $upload->errors();
		
		// Comprueba que hay un error en el campo filename
		$this->assertTrue( array_key_exists( 'filename', $errors));

		// Comprueba que no se ha guardado el upload
		$new = $this->Uploads->findById( $upload->id)->first();
		$this->assertTrue( $new === null);
	}


/**
 * Comprueba los métodos mágicos de la entity Upload\Model\Entity\Upload
 */
	public function testEntityGets()
	{
		$this->configure();

		$upload = $this->Uploads->get( 1);

		// Prueba de Upload::_getConfig()
		$config = $upload->config;
		$this->assertEquals( 'image', $config ['type']);
		$this->assertEquals( 100, $config ['config']['thumbnailQuality']);
		
		// Prueba de Upload::_getSinglePaths()
		$single_paths = $upload->singlePaths;
		$this->assertEquals( '10/20/30/thm_photo.jpg', $single_paths ['thm']);

		// Prueba de Upload::_getPaths()
		$paths = $upload->paths;
		$this->assertTrue( array_key_exists( 'thm', $paths));

		// Prueba de Upload::_getFullPaths()
		$full_paths = $upload->fullPaths;
		$this->assertTrue( array_key_exists( 'thm', $full_paths));

	}


	public function testResizeImage()
	{
		// Crea una imagen
		$this->configure();

		$temp_file = $this->filepath . 'woman2.jpg';
		copy( $this->filepath . 'woman.jpg', $this->filepath . 'woman2.jpg');

		$upload = $this->Uploads->newEntity([
			'filename' => [
				'name' => 'test.jpg',
        'type' => 'image/jpeg',
        'size' => 542,
        'tmp_name' => $temp_file,
        'error' => 0
			],
			'content_type' => 'post'
		]);


		$saved = $this->Uploads->save( $upload);
		$new = $this->Uploads->findById( $upload->id)->first();


		$upload = $this->Uploads->get( $new->id);

		$data = [
	    "id" => $upload->id,
      "path" => $upload->path,
      "width" => false,
      "height" => false,
      "size" => "square",
      "cropper" => [
          "x" => 0,
          "y" => 0,
          "width" => 400,
          "height" => 400
      ]
	  ];

	  // $this->Uploads->resizeImage( $data);
	}

	public function testSetCropData()
	{
		$content = $this->Uploads->get( 1);
    $data = $this->Uploads->setCropData( $content);

    $this->assertEquals( '100w', $data ['thumbails']['thm']['size']);
    $this->assertEquals( 'width: 40px; height: 20px; border-bottom: 4px dotted #ccc; margin-top: 20px;', $data ['configs']['thm']['style']);
    $this->assertEquals( 'width: 40px; height: 30px; margin-top: 10px;', $data ['configs']['hor']['style']);
    $this->assertEquals( 'width: 40px; height: 20px; border-bottom: 4px dotted #ccc; margin-top: 20px;', $data ['configs']['big']['style']);
    $this->assertEquals( 'Imagen de anchura fija a 800 píxeles', $data ['configs']['big']['help']);
    $this->assertEquals( 'Imagen de tamaño fijo a 400 x 300 píxeles', $data ['configs']['hor']['help']);
	}
}
