<?php

namespace Upload\Model\Behavior;

use ArrayObject;
use Cake\ORM\Table;
use Cake\ORM\Entity;
use Cake\Core\Plugin;
use Cake\Event\Event;
use Cake\ORM\Behavior;
use Cake\Core\Configure;
use Upload\Lib\UploadUtil;
use Cake\Filesystem\Folder;
use Cake\ORM\TableRegistry;
use Upload\Model\Entity\Asset;
use Upload\Model\Entity\Upload;
use Upload\Model\Entity\AssetArray;
use Cake\Datasource\EntityInterface;
use Cofree\Model\Behavior\JsonableTrait;

/**
 * UploadJsonable behavior
 */
class UploadJsonableBehavior extends Behavior
{

    use JsonableTrait;

    public function initialize(array $config)
    {
        $this->_defaultConfig = [
            'implementedMethods' => [
                'isJsonableField' => 'isJsonableField',
                'getUpload' => 'getUpload',
                'uploadsFields' => 'uploadsFields',
                'addUploadFields' => 'addUploadFields',
            ],
        ];
    }

    protected function _rowMapper($results)
    {
        return $results->map(function ($result) {
            foreach ($this->getConfig('fields') as $field) {
                if (isset($result->$field) && is_string($result->$field)) {
                    $array = json_decode($result->$field, true);

                    if (is_array($array)) {
                        if (is_numeric(key($array))) {
                            $value = new AssetArray();
                            
                            foreach ($array as $key => $data) {
                                if (isset($data['isUploading']) && $data['isUploading'] == 1) {
                                    continue;
                                }

                                if (!array_key_exists('error', $data)) {
                                    $value[] = new Asset($data);
                                }
                            }
                        } elseif (!empty($array)) {
                            if (!array_key_exists('error', $array)) {
                                $value = new Asset($array);
                            } else {
                                $result->set($field, null);
                            }
                        } elseif (empty($array)) {
                            $value = new AssetArray();
                        }
                    } else {
                        $value = null;
                    }

                    if (isset($value)) {
                        $result->set($field, $value);
                    }
                }
            }


            return $result;
        });
    }

    public function implementedEvents()
    {
        $events = parent::implementedEvents();
        $events['Crud.afterSetEmptyProperties'] = 'afterSetEmptyProperties';
        return $events;
    }

    public function afterSave(Event $event, EntityInterface $entity, ArrayObject $options)
    {
        foreach ($this->getConfig('fields') as $field) {
            /** @var \Cake\ORM\Entity $entity */

            if (is_array( $entity->getOriginal($field))) {

                $data = $entity->getOriginal($field);
                \Cake\Log\Log::debug( $data);
            }
        }
    }

    /**
     * Borra los uploads que han sido borrados desde la edición
     * 
     * @param  Event  $event
     * @param  Entity $entity
     * @return void
     */
    public function beforeMarshal(Event $event, ArrayObject $data)
    {
        foreach ($this->getConfig('fields') as $field) {
            if (isset($data[$field])) {
                $this->verifyType($field, $data);
                $data[$field] = json_encode($data[$field]);


                if ($data[$field] == '""') {
                    $data[$field] = null;
                }
            }
        }

        foreach ($this->getConfig('fields') as $field) {
            if (isset($data[$field])) {
                $records = $data[$field];

                if (is_array($records)) {
                    foreach ($records as $key => $record) {
                        if (!array_key_exists('id', $record)) {
                            unset($records[$key]);
                        }
                    }
                    $data[$field] = $records;
                }
            }
        }
    }

    private function verifyType($field, ArrayObject $data)
    {
        $config = $this->_table->crud->fieldCollector()->get($field);
        $values = $config->values();

        if (!isset($values['config'])) {
            return;
        }

        $config_upload = Configure::read('Upload.' . $values['config']['type']);

        if ($config_upload != 'image') {
            return;
        }

        if ($values['type'] == 'upload') {
            if ($values['config']['type'] != $data[$field]['content_type']) {
                $data[$field] = $this->resizeUpload($data[$field], $values);
            }
        } else {
            $uploads = $data[$field];

            if (empty($uploads)) {
                return;
            }

            foreach ((array)$uploads as &$upload) {
                if (array_key_exists('config', $values) && $values['config']['type'] != $upload['content_type']) {
                    $upload = $this->resizeUpload($upload, $values);
                }
            }

            $data[$field] = $uploads;
        }
    }


    private function resizeUpload($data_upload, $values)
    {
        $data_upload['content_type'] = $values['config']['type'];
        $upload = new Upload($data_upload);
        $config_upload = Configure::read('Upload.' . $values['config']['type']);
        $Uploads = TableRegistry::getTableLocator()->get('Upload.Uploads');

        if ($Uploads->hasBehavior('Upload')) {
            $Uploads->removeBehavior('Upload');
        }

        $Uploads->addBehavior('Upload.Upload', array(
            'filename' => $config_upload['config'],
        ));

        $_upload = $Uploads->find()
            ->where([
                'Uploads.id' => $data_upload['id']
            ])
            ->first();

        $alt_content_type = (array)$_upload->get('alt_content_type');

        if (!in_array($values['config']['type'], $alt_content_type)) {
            $Uploads->restoreThumbails($upload, 'filename', true);
        }

        $paths = UploadUtil::paths($upload);

        foreach ($paths as &$path) {
            $info = pathinfo($path);
            $path = str_replace($info['filename'], $info['filename'] . '_' . $values['config']['type'], $path);
        }

        $alt_content_type = (array)$_upload->get('alt_content_type');
        $alt_content_type[] = $values['config']['type'];
        $alt_content_type = array_unique($alt_content_type);

        TableRegistry::getTableLocator()->get('Upload.Uploads')->query()->update()
            ->set([
                'alt_content_type' => $alt_content_type
            ])
            ->where([
                'Uploads.id' => $_upload->id
            ])
            ->execute();

        $data_upload['paths'] = $paths;
        return $data_upload;
    }

    /**
     * Devuelve el upload de un campo, dado un id
     * 
     * @param  Entity $entity    La entidad
     * @param  string $field     El nombre del campo donde se encuentra el upload
     * @param  integer $upload_id El id del upload a buscar
     * 
     * @return Upload\Model\Entity\Asset | null          
     */
    public function getUpload($entity, $field, $upload_id)
    {
        foreach ($entity->$field as $upload) {
            if ($upload->id == $upload_id) {
                return $upload;
            }
        }

        return null;
    }

    public function uploadsFields()
    {
        return $this->getConfig('fields');
    }

    public function addUploadFields(array $fields)
    {
        $fields = array_merge($this->getConfig('fields'), $fields);
        $this->getConfig('fields', $fields);
    }

    public function existsUpload($upload_id)
    {
        foreach ($this->tables as $table => $fields) {
            $or = [];
            $Table = TableRegistry::getTableLocator()->get($table);

            foreach ($fields as $field) {
                if ($Table->hasField($field)) {
                    $or[] = "$field LIKE '%\"id\":{$upload_id->id},%'";
                }
            }

            $exists = $Table->find()
                ->where([
                    'OR' => $or
                ])
                ->first();

            if ($exists) {
                return true;
            }
        }

        return false;
    }

    private function getTables()
    {
        $tables = [];
        $plugins = Plugin::loaded();

        foreach ($plugins as $plugin) {
            $models = $this->getModels($plugin);

            foreach ($models as $model) {
                $table = TableRegistry::getTableLocator()->get($plugin . '.' . $model);

                if ($table->hasBehavior('UploadJsonable')) {
                    $fields = $table->uploadsFields();
                    $table_name = $table->getTable();

                    if (isset($tables[$table_name])) {
                        $tables[$table_name] = $tables[$table_name] + $fields;
                        $tables[$table_name] = array_unique($tables[$table_name]);
                    } else {
                        $tables[$table_name] = $fields;
                    }
                }
            }
        }

        $this->tables = $tables;
    }

    private function getModels($plugin)
    {
        $path = Plugin::path($plugin);
        $table_path = $path . 'src/Model/Table';

        $folder = new Folder($table_path);
        $tree = $folder->tree(null, false, 'file');
        $tree = array_map(function ($value) use ($table_path) {
            $value = str_replace($table_path . '/', '', $value);
            $value = str_replace('Table.php', '', $value);
            return $value;
        }, $tree);

        return $tree;
    }
}
