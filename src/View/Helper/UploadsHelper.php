<?php
namespace Upload\View\Helper;

use Cake\View\Helper;
use Cake\View\View;

/**
 * Uploads helper
 */
class UploadsHelper extends Helper
{

  /**
   * Default configuration.
   *
   * @var array
   */
  protected $_defaultConfig = [];

  public $helpers = ['Html', 'Cofree.Buffer', 'Form'];

/**
 * Devuelve una imagen
 *
 * Posible opciones para $options
 * - 'noimage' Si existe, devolverá una imagen en caso de que $upload esté vacio. Si se pasa una url a una imagen, devolverá esa imagen,
 *             Si no, devolverá la imagen por defecto
 * 
 * @param  string|object  $upload 
 * @param  string         $size     El key del tamaño de la imagen
 * @param  array          $attrs    Atributos HTML
 * @param  array          $options  Opciones
 * @return HTML
 */
  public function image( $upload, $size = 'big', $attrs = [], $options = [])
  {
    if( empty( $upload))
    {
      if( isset( $options ['noimage']))
      {
        if( $options ['noimage'] === true)
        {
          return $this->Html->image( '/upload/img/nophoto.jpg');
        }
        else
        {
          return $this->Html->image( $options ['noimage']);
        }
      }
    }
    if( is_string( $upload))
    {
      $upload = json_decode( $upload);
    }

    if( !is_object( $upload) && is_array( $upload))
    {
      $upload = json_decode( json_encode( $upload));
    }
    
    if( !empty( $upload->alt) && !array_key_exists( 'alt', $attrs))
    {
      $attrs ['alt'] = $upload->alt;
    }

    if( !empty( $upload->title) && !array_key_exists( 'title', $attrs))
    {
      $attrs ['title'] = $upload->title;
    }

    if( isset( $upload->paths->$size))
    {
      return $this->Html->image( $upload->paths->$size, $attrs);
    }
    elseif( isset( $options ['noimage']))
    {
      return $this->Html->image( '/upload/img/nophoto.jpg');
    }
    elseif( !empty( $upload->paths->org))
    {
      return $this->Html->image( str_replace( $upload->filename, $size .'_'. $upload->filename, $upload->paths->org), $attrs);
    }
  }

  public function filepath( $upload)
  {
    if( isset( $upload->paths [0]))
    {
      return $upload->paths [0];
    }
  }

/**
 * Escribe un enlace a un fichero
 * 
 * @param  Upload\Model\Entity\Asset $upload  
 * @param  string $before   El texto que aparecerá antes del nombre del fichero
 * @param  array  $attributes Atributos de la etiqueta <a>
 * @return string HTML
 */
  public function doc( $upload, $before = '', $attributes = [])
  {
    $_attributes = [
      'target' => '_blank'
    ];

    $attributes = array_merge( $_attributes, $attributes);
    $title = false;

    if( !empty( $upload->title))
    {
      $title = $upload->title;
    }

    $defaults = [
      'title' => $title,
      'escape' => false,
    ];

    if( !$title)
    {
      $title = $upload->filename;
    }

    $attributes = array_merge( $defaults, $attributes);

    if( $before)
    {
      $title = $before .' '. $title;
    }

    return $this->Html->link( $title, $upload->paths [0], $attributes);
  }


  public function findById( $photos, $id)
  {
    foreach( $photos as $photo)
    {
      if( $photo->id == $id)
      {
        return $photo;
      }
    }

    return false;
  }


/**
 * [uploadFront description]
 * @param  [type] $options
 *                   `key`
 *                   `uploadKey`
 *                   'ngController'
 *                   `multiple`
 * @return [type]             [description]
 */
  public function uploadFront( $content, array $options)
  {
    $out = [];
    $button_id = 'button_'. $options ['key'];
    $progress_id = 'progress_'. $options ['key'];
    $out [] = $this->Form->input($options ['key'], [
      'type' => 'hidden',
      'value' => '{{ '. $options ['key'] . '}}'
    ]);
    $out [] = '<input type="hidden" name="'. $options ['key'] .'" value="{{ '. $options ['key'] .' }}" />';

    if( $options ['multiple'])
    {
      // $out [] = '<div class="upload-photo" ng-repeat="photo in '. $options ['key'] .'"><img ng-src="{{ photo.paths.thm }}" /><a href ng-click="deletePhoto('."'". $options ['key'] ."'".', $index)"><i class="fa fa-remove"></i></a></div>';
    }
    else
    {
      $out [] = '<div class="upload-photo"><img ng-if="'. $options ['key'] .'" ng-src="{{ '. $options ['key'] .'.paths.thm }}" /></div>';
    }

    $out [] = $this->_View->element( 'Upload.uploads', [
      'multiple' => $options ['multiple'],
    ]);
    
    $out [] = '<div class="upload-button"><a id="' . $button_id . '" href="javascript:;"><i class="fa fa-upload"></i> ' . __d( 'app', 'Subir foto') . '</a></div>';
    
    if( !$options ['multiple'])
    {
      $out [] = '<div class="upload-delete"><a href ng-if="'. $options ['key'] .'"  ng-click="deletePhoto('."'". $options ['key'] ."'".')"><i class="fa fa-remove"></i>' . __d( 'app', 'Eliminar foto')  . '</a></div>';
    }

    $field = $content->{$options ['key']};

    if( !empty( $field) && is_string( $field))
    {
      $json = json_decode( $field, true);
      
      foreach( $json as $key => $asset)
      {
        $assets [] = [
          'asset' => $asset,
          'status' => 5,
          'id' => $key,
        ];
      }
      $upload = json_encode( $assets);
    }
    else
    {
      $upload = is_object( $field) ? json_encode( (array)$field->toArray()) : ($options ['multiple'] ? '[]' : 'null');
    }

    $this->Buffer->start();
    echo $this->_View->element( 'Upload.upload_script', [
      'upload' => $field,
      'uploads' => $upload,
      'upload_key' => $options ['uploadKey'],
      'progress_id' => $progress_id,
      'id' => $button_id,
      'multiple' => $options ['multiple'],
      'ng_controller' => $options ['ngController'],
      'key' => $options ['key']
    ]);
    $this->Buffer->end();

    return '<div class="upload-container">'. implode( "\n", $out) . '</div>';
  }


  public function uploadFrontOne( $content, array $options)
  {
    $out = [];
    $button_id = 'button_'. $options ['key'];
    $out [] = '<img ng-if="'. $options ['key'] .'" ng-src="{{ '. $options ['key'] .'.paths.thm }}" />';
    $out [] = '<input type="hidden" name="'. $options ['key'] .'" value="{{ '. $options ['key'] .' }}" />';
    $out [] = '<a id="' . $button_id . '" class="btn btn-default" href="javascript:;"><i class="fa fa-upload"></i> ' . __d( 'app', 'Subir foto') . '</a>';

    if( !$options ['multiple'])
    {
      $out [] = '<a href class="upload-delete" ng-if="'. $options ['key'] .'"  ng-click="deletePhoto('."'". $options ['key'] ."'".')"><i class="fa fa-remove"></i>' . __d( 'app', 'Eliminar foto')  . '</a>';
    }

    $this->Buffer->start();
    echo $this->_View->element( 'Upload.upload_script_one', [
      'upload' => $content->{$options ['key']},
      'upload_key' => $options ['uploadKey'],
      'id' => $button_id,
      'multiple' => $options ['multiple'],
      'ng_controller' => $options ['ngController'],
      'key' => $options ['key']
    ]);
    $this->Buffer->end();

    return implode( "\n", $out);
  }
}
