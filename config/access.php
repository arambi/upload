<?php

$config ['Access'] = [
  'uploads' => [
    'name' => 'Editar ficheros',
    'options' => [
      'view' => [
          'name' => 'Editar',
          'nodes' => [
            [
              'prefix' => 'admin',
              'plugin' => 'Upload',
              'controller' => 'Uploads',
              'action' => 'crop',
            ],
            [
              'prefix' => 'admin',
              'plugin' => 'Upload',
              'controller' => 'Uploads',
              'action' => 'proccess',
            ],
            [
              'prefix' => 'admin',
              'plugin' => 'Upload',
              'controller' => 'Uploads',
              'action' => 'edit',
            ],
          ]
      ]
    ]
  ]
];