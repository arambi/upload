<?php

namespace Upload\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\Event\Event;
use Cake\Database\Type;
use Upload\Lib\UploadUtil;
use Cake\Validation\Validator;
use Cake\Database\Schema\TableSchema;

Type::map('json', 'Cake\Database\Type\JsonType');

/**
 * Uploads Model
 */
class UploadsTable extends Table
{

    protected function _initializeSchema(TableSchema $schema)
    {
        $schema->setColumnType('alt_content_type', 'json');
        return $schema;
    }

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->setTable('uploads');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');
        $this->addBehavior('Manager.Crudable');

        $this->crud
            ->addIndex('index', [
                'fields' => [
                    'filename',
                ],
                'actionButtons' => ['create'],
                'saveButton' => false,
                'template' => 'Upload/upload_index'
            ])
            ->addView('crop', [
                'template' => 'Upload/crop',
                'title' => __d('admin', 'Recortar'),
                'columns' => [
                    [
                        'cols' => 12,
                        'box' => [
                            [
                                'title' => null,
                                'elements' => []
                            ]
                        ],
                    ],
                ],
                'saveButton' => true
            ])
            ->addView('edit', [
                'template' => 'Upload/edit',
                'title' => __d('admin', 'Editar'),
                'columns' => [
                    [
                        'cols' => 12,
                        'box' => [
                            [
                                'title' => null,
                                'elements' => []
                            ]
                        ],
                    ],
                ],
                'saveButton' => true
            ])
            ->addView('view', [
                'template' => 'Upload/upload_view',
                'title' => __d('admin', 'Información'),
                'columns' => [
                    [
                        'cols' => 12,
                        'box' => [
                            [
                                'title' => null,
                                'elements' => []
                            ]
                        ],
                    ],
                ],
                'saveButton' => true
            ])
            ->setName([
                'singular' => __d('admin', 'Fotografía'),
                'plural' => __d('admin', 'Fotografías'),
            ]);

        $this->crud->limit(50);

        $this->crud->defaults([
            'cols' => 12
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator
     * @return \Cake\Validation\validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->notEmpty('content_type')
            ->allowEmpty('filename')
            ->allowEmpty('path')
            ->allowEmpty('original_filename')
            ->add('filesize', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('filesize')
            ->allowEmpty('extension')
            ->allowEmpty('mimetype')
            ->allowEmpty('duration');

        return $validator;
    }

    public function deleteDB($upload)
    {
        $upload->deleted = 1;
        if($this->save($upload)){
            return true;
        }

        return false;
    }

    public function deleteUpload($upload_id)
    {
        $upload = $this->findById($upload_id)->first();

        if (!$upload) {
            return;
        }

        foreach ($upload->full_paths as $path) {
            if (file_exists($path)) {
                unlink($path);
            }
        }

        $this->delete($upload);
    }

    /**
     * Devuelve los datos para que la foto sea recortada
     * 	
     * @param Upload\Model\Entity\Upload $upload
     */
    public function setCropData($upload)
    {
        $size = false;

        foreach ($upload->config['config']['thumbnailSizes'] as $_size => $thm_info) {
            if (is_array($thm_info)) {
                $thumbails[$_size] = $thm_info;

                if (!$size) {
                    $size = $_size;
                }
            }
        }

        $configs = array();

        foreach ($thumbails as $_size => $info) {
            if (strpos($info['size'], 'x')) {
                list($width, $height) = explode('x', $info['size']);

                $help = __d('admin', 'Imagen de tamaño fijo a %s x %s píxeles', [$width, $height]);
                $name = $width . 'x' . $height;

                if ($width == $height) {
                    $style = 'width: 40px; height: 40px';
                } elseif ($width > $height) {
                    $style = 'width: 40px; height: ' . round((40 * $height) / $width) . 'px; margin-top: ' . (40 - round((40 * $height) / $width)) . 'px;';
                } else {
                    $style = 'width: ' . round((40 * $width) / $height) . 'px; height: 40px';
                }
            } elseif (strpos($info['size'], 'w')) {
                $width = false;
                $height = false;
                $w = str_replace('w', '', $info['size']);

                $style = 'width: 40px; height: 20px; border-bottom: 4px dotted #ccc; margin-top: 20px;';
                $help = __d('admin', 'Imagen de anchura fija a %s píxeles', [$w]);
                $name = $w . ' x ' . '?';
            } elseif (strpos($info['size'], 'h')) {
                $width = false;
                $height = false;
                $h = str_replace('h', '', $info['size']);
                $style = 'width: 20px; height: 40px; border-right: 4px dotted #ccc;';
                $help = __d('admin', 'Imagen de altura fija a %s píxeles', [$h]);
                $name = '?' . ' x ' . $h;
            }

            $configs[$_size] = array(
                'id' => $upload->id,
                'path' => $upload->path,
                'width' => $width,
                'height' => $height,
                'size' => $_size,
                'style' => $style,
                'name' => $name,
                'help' => $help
            );
        }

        return [
            'thumbails' => $thumbails,
            'configs' => $configs,
            'current' => $configs[$size]
        ];
    }

    /**
     * Recorta una imagen dado unos datos con un array
     * 
     * @param  integer $data ['id'] El id del upload
     * @param  string $data ['size'] El key del tamaño de la foto
     * @param  integer $data ['cropper']['width'] El ancho de la foto
     * @param  integer $data ['cropper']['height'] La altura de la foto
     * @param  integer $data ['cropper']['x'] La posicion X del corte de la foto
     * @param  integer $data ['cropper']['y'] La posicion Y del corte de la foto
     * 
     * @return void
     */
    public function resizeImage($data)
    {
        $upload = $this->get($data['id']);

        // La configuración del upload
        $config = $upload->config;
        $config['config']['fields'] = array(
            'dir' => 'path',
            'type' => 'mimetype'
        );

        if ($this->hasBehavior('Upload')) {
            $this->removeBehavior('Upload');
        }

        $this->addBehavior('Upload.Upload', [
            'filename' => $config['config']
        ]);

        $file = $this->crop($upload, $data['cropper']);
        $config = $upload->config;
        $geometry = $config['config']['thumbnailSizes'][$data['size']]['size'];

        $pathinfo = pathinfo($file);
        $this->resizeThmPhp($upload, $file, 'filename', $pathinfo['dirname'] . DS, $data['size'], $geometry, $pathinfo['dirname'] . DS, $upload->filename);
        unlink($file);
    }

    /**
     * Recorta una imagen dado unos datos con un array
     *
     * @param  Upload\Model\Entity\Upload $upload
     * @param  integer $data ['width'] El ancho de la foto
     * @param  integer $data ['height'] La altura de la foto
     * @param  integer $data ['x'] La posicion X del corte de la foto
     * @param  integer $data ['y'] La posicion Y del corte de la foto
     * 
     * @return string
     */
    public function crop($upload, $data)
    {
        $imagepath = substr(WWW_ROOT, 0, -1) . UploadUtil::filepath($upload);

        $pathinfo = pathinfo($imagepath);

        $type = exif_imagetype($imagepath);

        list($width, $height) = getimagesize($imagepath);

        switch ($type) {
            case IMAGETYPE_GIF:
                $src_img = imagecreatefromgif($imagepath);
                $outputHandler = 'imagegif';
                break;
            case IMAGETYPE_JPEG:
                $src_img = imagecreatefromjpeg($imagepath);
                $outputHandler = 'imagejpeg';
                break;
            case IMAGETYPE_PNG:
                $src_img = imagecreatefrompng($imagepath);
                $outputHandler = 'imagepng';
                break;
        }

        $img = imagecreatetruecolor($data['width'], $data['height']);
        $result = imagecopy($img, $src_img,  0, 0, $data['x'], $data['y'], $data['width'], $data['height']);

        $quality = $outputHandler == 'imagepng' ? 9 : 100;

        $destFile = $pathinfo['dirname'] . DS . $pathinfo['filename'] . '_resample.' . $pathinfo['extension'];
        $outputHandler($img, $destFile, $quality);

        return $destFile;
    }
}
