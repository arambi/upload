<?php
namespace Upload\Model\Entity;

use I18n\Lib\Lang;
use Cake\ORM\Entity;
use Cake\Core\Configure;
use Cake\Routing\Router;
use Cofree\Lib\UserAgent;
use Upload\Lib\UploadUtil;
use Cake\Utility\Inflector;
use Upload\Model\Entity\Path;
use Manager\Model\Entity\CrudEntityTrait;

/**
 * Upload Entity.
 */
class Asset extends Entity 
{

/**
 * Fields that can be mass assigned using newEntity() or patchEntity().
 *
 * @var array
 */
  protected $_accessible = [
    'content_type' => true,
    'filename' => true,
    'path' => true,
    'original_filename' => true,
    'filesize' => true,
    'extension' => true,
    'mimetype' => true,
    'duration' => true,
    'locale' => true,
    'paths' => true
  ];

  protected $_virtual = [
    'title',
    'alt'
  ];

/**
 * Método mágico para tomar la propiedad paths
 *
 * Devuelve los paths (urls) del fichero
 *  
 * @return array
 */
  protected function _setPaths( $value)
  {
    if( !is_array( $value))
    {
      return null;
    }

    return new Path( $value);
  }

  protected function _getPaths($value)
	{
    if ($value !== null) {
      return $value;
    }
    
		return UploadUtil::paths( $this);
	}

  protected function _getTitle()
  {
    return $this->title();
  }

  protected function _getAlt()
  {
    return $this->alt();
  }

  public function filesize()
  {
    $bytes = $this->filesize;

    if ($bytes >= 1073741824)
    {
      $bytes = number_format( $bytes / 1073741824, 1) . ' GB';
    }
    elseif ($bytes >= 1048576)
    {
      $bytes = number_format( $bytes / 1048576, 1) . ' MB';
    }
    elseif ($bytes >= 1024)
    {
      $bytes = number_format( $bytes / 1024) . ' KB';
    }
    elseif ($bytes > 1)
    {
      $bytes = $bytes . ' bytes';
    }
    elseif ($bytes == 1)
    {
      $bytes = $bytes . ' byte';
    }
    else
    {
      $bytes = '0 bytes';
    }

    return $bytes;
  }

/**
 * Retorna el título del upload
 * 
 * @return string
 */
  public function title( $return_filename = true)
  {
    $locale = Lang::current( 'iso3');

    if( isset( $this->$locale))
    {
      $texts = $this->$locale;
      if( !empty( $texts ['title']))
      {
        return $texts ['title'];
      }
    }

    
    if( empty( $title))
    {
      foreach( Lang::iso3() as $iso3 => $locale)
      {
        if( isset( $this->$iso3))
        {
          $texts = $this->$iso3;
        }

        if( !empty( $texts ['title']))
        {
          return $texts ['title'];
        }
      }
    }

    if( $return_filename)
    {
      return $this->filename;
    }
  }

  public function alt()
  {
    foreach( Lang::iso3() as $iso3 => $locale)
    {
      if( isset( $this->$iso3))
      {
        $texts = $this->$iso3;
      }

      if( !empty( $texts ['alt']))
      {
        return $texts ['alt'];
      }
    }
  }

  public function path( $size = 0)
  {
    return $this->paths->$size;
  }


  public function download( $response, $size = 0, $title = null)
  {
    $file = fopen( WWW_ROOT . $this->path( $size), 'r');
    $response->body( function () use ( $file) {
        rewind( $file);
        fpassthru( $file);
        fclose( $file);
    });

    $response->type( $this->mimetype);

    if( $title === null)
    {
      $title = $this->title();
    }

    $response->download( Inflector::slug( $title) .'.'. $this->extension);

    return $response;
  }

  public function image( $size = 'big', $attrs = [], $options = [])
  {
    $upload = $this;
    
    if( !is_object( $upload) && is_array( $upload))
    {
      $upload = json_decode( json_encode( $upload));
    }
    
    if( !empty( $upload->alt) && !array_key_exists( 'alt', $attrs))
    {
      $attrs ['alt'] = $upload->alt;
    }

    if( !empty( $upload->title) && !array_key_exists( 'title', $attrs))
    {
      $attrs ['title'] = $upload->title;
    }

    if( isset( $upload->paths->$size))
    {
      $filepath = $upload->paths->$size;

      if( $this->isMobile())
      {
        $mobile_path = str_replace( $size .'_'. $upload->filename, 'mobile_'. $size .'_'. $upload->filename, $filepath);
       
        if( file_exists( WWW_ROOT . $mobile_path))
        {
          $filepath = $mobile_path;
        }
      }

      return $this->__tag( $filepath, $attrs);
    }
    elseif( isset( $options ['noimage']))
    {
      return $this->__tag( '/upload/img/nophoto.jpg');
    }
    elseif( !empty( $upload->paths->org))
    {
      return $this->__tag( str_replace( $upload->filename, $size .'_'. $upload->filename, $upload->paths->org), $attrs);
    }
  } 

  public function imageUrl( $size = 'big', $mobile = true)
  {
    if( isset( $this->paths->$size))
    {
      $filepath = $this->paths->$size;

      if( $mobile && $this->isMobile())
      {
        $mobile_path = str_replace( $size .'_'. $this->filename, 'mobile_'. $size .'_'. $this->filename, $filepath);
       
        if( file_exists( WWW_ROOT . $mobile_path))
        {
          $filepath = $mobile_path;
        }
      }

      return Router::url($filepath, true);
    }
  }

  public function imageBg( $size = 'big')
  {
    $image = $this->imageUrl( $size);

    if( $image)
    {
      return 'style="background-image: url(\''. $image .'\')"'; 
    }
  }

  private function __tag( $file, $attrs = [])
  {
    if( Configure::read( 'Asset.lazy'))
    {
      if( isset( $attrs ['class']))
      {
        $attrs ['class'] .= ' lozad';
      }
      else
      {
        $attrs ['class'] = 'lozad';
      }

      $src = Configure::read( 'Asset.lazyLoading') ? 'src="'. Configure::read( 'Asset.lazyLoading') .'" ' : '';
      $img = '<img '. $src . $this->attrs( $attrs) .' data-src="' . $file .'" />';
    }
    else
    {
      $img = '<img src="' . $file .'"'. $this->attrs( $attrs) .' />';
    }

    if( array_key_exists( 'span', $attrs))
    {
      $img = '<span class="img" '. (!empty( $attrs ['title']) ? 'title="'. $attrs ['title'] . '"' : '') .'>'. $img .'</span>';
    }

    return $img;
  }

  private function attrs( $attrs)
  {
    $return = '';

    foreach( ['title', 'alt', 'style', 'itemprop', 'class'] as $name)
    {
      if( !empty( $attrs [$name]))
      {
        $return .= ' '. $name . '="' . $attrs [$name] . '"';
      }
    }

    return $return;
  }

  private function isMobile()
  {
    return UserAgent::isMobile();
  }

  public function getDimensions($size)
  {
    $path = WWW_ROOT . $this->path( $size);
    $size = @getimagesize($path);

    if ($size) {
      return [
        'width' => $size[0],
        'height' => $size[1],
      ];
    } 
  }

  public function isHorizontal($size)
  {
    $dimensions = $this->getDimensions($size);

    if ($dimensions) {
      return $dimensions['width'] > $dimensions['height'];
    }
  }
}
