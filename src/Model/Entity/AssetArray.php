<?php

namespace Upload\Model\Entity;

use ArrayObject;
use I18n\Lib\Lang;
use Cake\ORM\Entity;
use JsonSerializable;
use Cake\Core\Configure;
use Upload\Lib\UploadUtil;
use Cake\Utility\Inflector;
use Upload\Model\Entity\Path;
use Manager\Model\Entity\CrudEntityTrait;

/**
 * Upload Entity.
 */
class AssetArray extends ArrayObject implements JsonSerializable
{

    public function jsonSerialize() 
    {
        return (array)$this;
    }

    public function __toString()
    {
        $array = [];

        foreach ((array)$this as $child) {
            $array[] = $child->toArray();
        }
        
        return json_encode($array);
    }

    public function first()
    {
        return $this[0];
    }

    public function currentLocale()
    {
        $locale = Lang::current('iso3');

        foreach ($this as $asset) {
            if (empty($asset->locale) || $asset->locale == $locale) {
                return $asset;
            }
        }
    }

    public function currentLocaleMultiple()
    {
        $locale = Lang::current('iso3');

        foreach ($this as $asset) {
            \Cake\Log\Log::debug( '------------------');
                \Cake\Log\Log::debug( $asset);
                \Cake\Log\Log::debug( '------------------');
            if ($asset->locales) {
                $locales = $asset->locales;
                
                $locales = array_filter($locales, function ($value) {
                    return $value;
                });

                if (array_key_exists($locale, $locales)) {
                    return $asset;
                }
            }
        }
    }

    public function locales()
    {
        $locale = Lang::current('iso3');
        $return = [];


        foreach ($this as $key => $asset) {
            if ($asset->locale && $asset->locale == $locale) {
                $return[] = $asset;
                continue;
            }

            if ($asset->locales) {
                $locales = $asset->locales;

                $locales = array_filter($locales, function ($value) {
                    return $value;
                });

                if (is_array($locales) && array_key_exists($locale, $locales)) {
                    $return[] = $asset;
                }
            }
        }

        return $return;
    }
}
