<div class="upload-asset-container">
  <div ng-cloak ng-repeat="upload in uploads" class="upload-asset">
    <div ng-show="upload.status == 5" class="upload-photo">
      <img ng-src="{{ upload.asset.paths.thm }}" />
      <a href ng-click="deletePhoto( upload.id)"><i class="fa fa-remove"></i></a>
    </div>
    <div ng-show="upload.status == 4" class="upload-error">
      error
    </div>
    <div ng-show="upload.status == 2" class="upload-progress">
      <div class="upload-progress-spinner"><i class="fa fa-spin fa-spinner"></i></div>
      <div class="upload-progress-percent">
        <span style="width: {{ upload.percent }}%"></span>
      </div>
    </div>
  </div>
</div>