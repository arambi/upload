<div class="row wrapper border-bottom white-bg page-heading">
  <ng-include src="'header.html'"></ng-include>
</div>
<div class="animated fadeIn">
  <div class="row">
      <div class="col-xs-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          <div class="panel-options">
            <ul class="nav nav-tabs pl-crop-thms">
              <li ng-repeat="(size, thm) in data.configs" ng-class="{active: (data.current.size == size)}" im-get="{{ size }}">
                <a tooltip="{{ thm.help }}" href><div class="pl-crop-thm-box" style="{{ thm.style }}"></div><br>{{ thm.name }}</a>
              </li>
            </ul>
          </div>
          <div class="panel-body">
            <div class="col-sm-5">
              <h4><?= __d( 'admin', 'Imagen original') ?></h4>
              <div class="im-cropper-container">
                <img  im-cropper 
                    bt-submit="#submit" 
                    ng-model="data.current.cropper" 
                    preview=".img-preview" 
                    src="{{ data.content.paths.org }}"
                    style="height: {{ maxHeight }}">
              </div>
            </div>
            <div class="col-sm-5 clearfix">
              <h4><?= __d( 'admin', 'Resultado') ?></h4>
              <div style="height: 400px" class="img-preview"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>