<?php
use Migrations\AbstractMigration;

class AltContentType extends AbstractMigration
{
  public function change()
  {
    $uploads = $this->table( 'uploads');

    if( !$uploads->hasColumn( 'alt_content_type'))
    {
      $uploads
        ->addColumn( 'alt_content_type', 'text')
        ->update();
    }
  }
}
