<div class="row wrapper border-bottom white-bg page-heading">
  <ng-include src="'header.html'"></ng-include>
</div>
<div class="animated fadeIn">
  <div class="row">
      <div class="col-xs-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          <div class="panel-body">
            <div class="form-group">
              <label class="col-lg-3 control-label"><?= __d( 'admin', 'Título') ?></label>
              <div class="col-lg-9">
                <div ng-class="{'input-group': data.languages.length > 1}">
                  <set-languages></set-languages>
                  <input ng-show="data.currentLanguage.iso3 == lang.iso3" ng-repeat="lang in data.languages" ng-model="plFilesModel[lang.iso3].title" type="text" class="form-control">
                </div>
              </div>
            </div>
            <div class="form-group">
              <label class="col-lg-3 control-label"><?= __d( 'admin', 'Alt') ?></label>
              <div class="col-lg-9">
                <div ng-class="{'input-group': data.languages.length > 1}">
                  <set-languages></set-languages>
                  <input ng-show="data.currentLanguage.iso3 == lang.iso3" ng-repeat="lang in data.languages" ng-model="plFilesModel[lang.iso3].alt" type="text" class="form-control">
                </div>
              </div>
            </div>
            <div class="form-group">
              <label class="col-lg-3 control-label"><?= __d( 'admin', 'Autor') ?></label>
              <div class="col-lg-9">
                <div>
                  <input ng-model="plFilesModel.author" type="text" class="form-control">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

