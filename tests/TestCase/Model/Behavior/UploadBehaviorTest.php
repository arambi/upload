<?php
namespace Upload\Test\TestCase\Model\Behavior;

use Upload\Model\Behavior\UploadBehavior;
use Cake\TestSuite\TestCase;
use Cake\ORM\Query;
use Cake\ORM\Table;

/**
 * Upload\Model\Behavior\UploadBehavior Test Case
 */
class UploadBehaviorTest extends TestCase {

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {

		parent::tearDown();
	}

/**
 * Test initial setup
 *
 * @return void
 */
	public function testInitialization() {

	}

}
