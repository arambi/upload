<?php
namespace Upload\Shell;

use Cake\Console\Shell;
use Cake\Core\Configure;

ini_set('memory_limit', '512M');

/**
 * Restore shell command.
 */
class RestoreShell extends Shell
{

  /**
   * Manage the available sub-commands along with their arguments and help
   *
   * @see http://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
   *
   * @return \Cake\Console\ConsoleOptionParser
   */
  public function getOptionParser()
  {
      $parser = parent::getOptionParser();

      return $parser;
  }

  /**
   * main() method.
   *
   * @return bool|int Success or error code.
   */
  public function main() 
  {
    $config = Configure::read( 'Upload');
    $keys = [];
    $i = 1;

    foreach( $config as $key => $values)
    {
      if( $values ['type'] == 'image')
      {
        $keys [$i] = $key;
        $this->out( $i .'. '. $key);
        $i++;
      }
        
    }

    $key = $this->in( 'Indica un tipo de imagen');

    if( !array_key_exists( $key, $keys))
    {
      $this->error( 'No existe el tipo de imagen seleccionado');
    }

    $this->loadModel( 'Upload.Uploads');

    $config = Configure::read( 'Upload.'. $keys [$key]);

    $this->Uploads->addBehavior( 'Upload.Upload', array( 
        'filename' => $config ['config'],
    ));

    $uploads = $this->Uploads->find()
      ->order([
        'created' => 'desc'
      ])
      ->where([
        'content_type' => $keys [$key] 
      ]);

    foreach( $uploads as $upload)
    {
      $this->Uploads->restoreThumbails( $upload, 'filename');
      $this->out( 'Restaurando foto '. $upload->id . ' - ' . $upload->path . '/'. $upload->filename);
    }
  }
}
