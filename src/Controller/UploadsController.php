<?php
namespace Upload\Controller;

use Cake\Utility\Text;
use Cake\Core\Configure;
use Upload\Controller\AppController;

/**
 * Uploads Controller
 *
 * @property Upload\Model\Table\UploadsTable $Uploads
 */
class UploadsController extends AppController 
{

	public function initialize()
  {
    parent::initialize();
    
    if( isset( $this->Auth))
    {
      $this->Auth->allow( ['upload', 'add', 'crop', 'uploadfile']);
    }

    $this->loadComponent( 'RequestHandler');
    $this->loadComponent( 'Upload.FolderPath');
  }
  
  public function upload()
  {
    // La configuración del upload
    $config = Configure::read( 'Upload.'. $this->request->query ['key']);
    // $config ['config']['fields'] = array(
    //     'dir' => 'path',
    //     'type' => 'mimetype'
    // );

    if( !array_key_exists( 'config', $config))
    {
      $config ['config'] = [];
    }

    // Lee el Behavior Upload.Upload para el model asociado, pasándole los datos indicados en la configuración anteriormente leída
    $this->Uploads->addBehavior( 'Upload.Upload', array( 
        'filename' => $config ['config'],
    ));
    
    // Los datos a guardar
    $data = $this->request->data;
    $data ['content_type'] = $this->request->query ['key'];
    
    $upload = $this->Uploads->newEntity( $data);

    if( $this->Uploads->save( $upload))
    {
      $this->set( 'success', true);
      $upload = $this->Uploads->findById( $upload->id)->first();
      $upload->set( 'paths', $upload->paths);

      if( isset( $config ['thumbnailSizes']))
      {
        $last ['Upload']['thumbail'] = UploadUtil::thumbailPathMulti( $last);
      }
      
      $this->set( 'upload', $upload->toArray());
      $this->set( '_serialize', array( 'success', 'upload'));
    }
    else
    {
      $errors = $upload->errors();
      
      if( isset( $errors ['filename']))
      {
        $this->set( 'error', current( $errors ['filename']));
        $filename = json_decode( $upload->filename, true);
        $upload->filename = $filename ['name'];
      }
      else
      {
        $this->set( 'error', false);
      }

      $this->set( 'upload', $upload->toArray());

      $this->set( 'success', false);
      $this->set( '_serialize', array( 'success', 'error', 'upload'));
    }
  }

  

  public function uploadfile()
  {
    $this->viewBuilder()->layout( 'ajax');

    if( isset( $this->request->data ['upload']['tmp_name']))
    {
      $upload = $this->request->data ['upload'];
      $path = $this->FolderPath->getPath();

      $ext = pathinfo( $upload ['name'], PATHINFO_EXTENSION);

      $filename = strtolower( Text::slug( pathinfo( $upload ['name'], PATHINFO_FILENAME))) .'.'. $ext;

      $dir = $path ['serverPath'] .DS. $filename;

      if( $this->__moveUploadedFile( $upload ['tmp_name'], $dir))
      {
        $this->set( 'url', $path ['urlPath'] .'/'. $filename);
        $this->set( 'funcNum', $this->request->query ['CKEditorFuncNum']);
      }
    }
  }
  
  
  private function __moveUploadedFile( $tmp, $filePath)
  {
    if( is_uploaded_file( $tmp))
    {
      return @move_uploaded_file( $tmp, $filePath);
    }
    
    return false;
  }




}
