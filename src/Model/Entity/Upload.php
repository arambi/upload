<?php
namespace Upload\Model\Entity;

use Cake\ORM\Entity;
use Upload\Lib\UploadUtil;
use Cake\Core\Configure;
use Manager\Model\Entity\CrudEntityTrait;
use I18n\Lib\Lang;

/**
 * Upload Entity.
 */
class Upload extends Entity 
{
	use CrudEntityTrait;
/**
 * Fields that can be mass assigned using newEntity() or patchEntity().
 *
 * @var array
 */
	protected $_accessible = [
		'*' => true,
	];

/**
 * Método mágico para tomar la propiedad singlePaths
 *
 * Devuelve los paths del fichero sin tener en cuenta el directorio principal
 * Es decir si el fichero se encuentra en /files/20/30/40/thm_photo.jpg, devolverá solo 20/30/40/thm_photo.jpg
 *  
 * @return array
 */
	public function _getSinglePaths()
	{
		$paths = UploadUtil::paths( $this);

		$return = [];

		foreach( $paths as $key => $path)
		{
			$return [$key] = str_replace( '/'. Configure::read( 'App.filesBaseUrl'), '', $path);
		}

		return $return;
	}

/**
 * Método mágico para tomar la propiedad paths
 *
 * Devuelve los paths (urls) del fichero
 * 	
 * @return array
 */
	protected function _getPaths()
	{

		return UploadUtil::paths( $this);
	}

/**
 * Método mágico para tomar la propiedad fullPaths
 *
 * Devuelve los paths (de servidor) del fichero
 * 	
 * @return array
 */
	protected function _getFullPaths()
	{
		$paths = UploadUtil::paths( $this);

		$return = [];

		foreach( $paths as $key => $path)
		{
			$return [$key] = substr( WWW_ROOT, 0, -1) . $path;
		}

		return $return;
	}

	protected function _getConfig()
	{
		return UploadUtil::getConfig( $this);
	}


/**
 * Retorna el título del upload
 * 
 * @return string
 */
	public function title()
	{
		$locale = Lang::current( 'iso3');

		if( isset( $this->$locale))
		{
			$texts = $this->$locale;
			if( !empty( $texts ['title']))
			{
				return $texts ['title'];
			}
		}

		
		if( empty( $title))
		{
			foreach( Lang::iso3() as $iso3 => $locale)
			{
				if( isset( $this->$iso3))
				{
					$texts = $this->$iso3;
				}

				if( !empty( $texts ['title']))
				{
					return $texts ['title'];
				}
			}
		}

		return $this->filename;
	}

}
