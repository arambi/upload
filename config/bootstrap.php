<?php
use Cake\Core\Configure;


if( file_exists( ROOT .DS. 'config' .DS. 'upload.php'))
{
  Configure::load( 'upload');
}

if( class_exists( '\User\Auth\Access'))
{

  \User\Auth\Access::add( 'uploads', [
    'name' => 'Editar ficheros',
    'options' => [
      'view' => [
          'name' => 'Editar',
          'nodes' => [
            [
              'prefix' => 'admin',
              'plugin' => 'Upload',
              'controller' => 'Uploads',
              'action' => 'index',
            ],
            [
              'prefix' => 'admin',
              'plugin' => 'Upload',
              'controller' => 'Uploads',
              'action' => 'crop',
            ],
            [
              'prefix' => 'admin',
              'plugin' => 'Upload',
              'controller' => 'Uploads',
              'action' => 'proccess',
            ],
            [
              'prefix' => 'admin',
              'plugin' => 'Upload',
              'controller' => 'Uploads',
              'action' => 'edit',
            ],
            [
              'prefix' => 'admin',
              'plugin' => 'Upload',
              'controller' => 'Uploads',
              'action' => 'view',
            ],
          ]
      ]
    ]
  ]);
}