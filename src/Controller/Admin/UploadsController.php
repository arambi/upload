<?php
namespace Upload\Controller\Admin;

use Cake\Core\Plugin;
use Cake\Http\Client;
use Cake\Core\Configure;
use Cake\Routing\Router;
use Cake\Filesystem\Folder;
use Cake\ORM\TableRegistry;
use Upload\Controller\AppController;
use Manager\Controller\CrudControllerTrait;

/**
 * Uploads Controller
 *
 * @property \Upload\Model\Table\UploadsTable $Uploads
 */
class UploadsController extends AppController
{
  use CrudControllerTrait;

  public function _index( $query)
  {
    $key = $this->getRequest()->getParam( 'pass.0');
    $content_type = $this->getRequest()->getParam( 'pass.1');
    $type = $this->getRequest()->getParam( 'pass.2');
    $config = Configure::read( 'Upload.'. $content_type);
    $keys = [];

    foreach( Configure::read( 'Upload') as $_key => $data)
    {
      if( $data ['type'] == $config ['type'])
      {
        $keys [] = $_key;
      }
    }

    $query->formatResults( function( $result){
      return $result->map( function( $row){
        $row->set( 'paths', $row->path);
        return $row;
      });
    })
    ->where([
      'Uploads.content_type IN' => $keys,
      'deleted' => 0
    ])
    ->order(['created' => 'DESC']);

    $this->CrudTool->addSerialized([
      'uploadField' => $key,
      'uploadType' => $type,
      'fileType' => $config ['type'],
    ]);
  }

  public function view( $id)
  {
    $content = $this->Uploads->get( $id);
    $content->set( 'paths', $content->paths);
    $this->Uploads->crud->setContent( $this->Uploads->emptyEntityProperties( $content));

    $this->getTables();
    $links = $this->getRoutes( $content);
    
    $this->CrudTool->addSerialized([
      'links' => $links
    ]);
  }

  private function getRoutes( $upload)
  {
    $client = new Client();
    $out = [];
    $links = [];
    
    foreach( $this->tables as $table => $fields)
    {
      $or = [];
      $Table = TableRegistry::get( $table);

      foreach( $fields as $field)
      {
        if( $Table->hasField( $field))
        {
          $or [$field .' LIKE'] = '%"id":'. $upload->id . ',%';
        }
      }
      
      $content = $Table->find()
        ->where([
          'OR' => $or
        ])
        ->first();
      
      if( $content)
      {
        list( $plugin, $controller) = explode( '.', $content->getSource());
        $link = $content->link();

        try {
          $response = $client->get( $link);

          if( $response->getStatusCode() > 301)
          {
            $link = false;
          }
        } catch (\Throwable $th) {
          $link = false;  
        }

        if( !in_array( $link, $links))
        {
          $out [] = [
            'front' => $link,
            'admin' => [
              'title' => $Table->crud->getName()['plural'] .' / '. $content->get( $Table->getDisplayField()),
              'url' => Router::url([
                'plugin' => $plugin,
                'controller' => $controller,
                'action' => 'update',
                $content->get( $Table->getPrimaryKey())
              ])
            ]
          ];

          $links [] = $link;
        }
      }
    }

    return array_values( $out);
  }

  private function getTables()
  {
    $tables = [];
    $plugins = Plugin::loaded();

    foreach( $plugins as $plugin)
    {
      $models = $this->getModels( $plugin);

      foreach( $models as $model)
      {
        if( !$this->hasController( $plugin, $model))
        {
          continue;
        }

        $table = TableRegistry::get( $plugin.'.'. $model);
        
        if( $table->hasBehavior( 'UploadJsonable'))
        {
          $fields = $table->uploadsFields();
          $table_name = $plugin.'.'. $model;

          if( isset( $tables [$table_name]))
          {
            $tables [$table_name] = $tables [$table_name] + $fields;
            $tables [$table_name] = array_unique( $tables [$table_name]);
          }
          else
          {
            $tables [$table_name] = $fields;
          }
        }
      }
    }

    $this->tables = $tables;
  }

  private function hasController( $plugin, $model)
  {
    $path = Plugin::path( $plugin);
    $controller_path = $path .'src/Controller/Admin/' . $model .'Controller.php';
    return file_exists( $controller_path);
  }

  private function getModels( $plugin)
  {
    $path = Plugin::path( $plugin);
    $table_path = $path .'src/Model/Table';

    $folder = new Folder( $table_path);
    $tree = $folder->tree( null, false, 'file');
    $tree = array_map( function( $value) use ($table_path) {
      $value = str_replace( $table_path . '/', '', $value);
      $value = str_replace( 'Table.php', '', $value);
      return $value;
    }, $tree);

    return $tree;
  }
  

/**
 * Se encarga de preparar las fotos para ser cortadas
 *
 * Recoge el id mediante post
 */
  public function crop()
  {
    $content = $this->Uploads->get( $this->request->data ['id']);
    $content->set( 'paths', $content->paths);
    $this->Uploads->crud->setContent( $this->Uploads->emptyEntityProperties( $content));

    $data = $this->Uploads->setCropData( $content);
    
    $this->CrudTool->addSerialized([
      'thumbails' => $data ['thumbails'],
      'configs' => $data ['configs'],
      'current' => $data ['current']
    ]);
  }

  public function edit()
  {
    $content = $this->request->data ['upload'];
    $type = Configure::read( 'Upload.'. $content ['content_type']);
    $content = $this->Uploads->setTranslatedFields( $content, ['title', 'text']);
    $this->Uploads->crud->setContent( $content);
    $this->CrudTool->addSerialized([
      'uploadType' => $type
    ]);
  }


  public function proccess()
  {
    $data = $this->request->data ['upload'];
    $this->Uploads->resizeImage( $data);
  }
}
