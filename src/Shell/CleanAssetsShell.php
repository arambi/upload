<?php

namespace Upload\Shell;

use Cake\Core\Plugin;
use Cake\Console\Shell;
use Cake\Core\Configure;
use Cake\Filesystem\Folder;
use Cake\ORM\TableRegistry;

/**
 * CleanAssets shell command.
 */
class CleanAssetsShell extends Shell
{

    private $tables = [];

    public function main()
    {
        $this->getTables();
        $this->iterateUploads();
    }

    public function deleteFromDb()
    {
        $uploads = $this->loadModel('Upload.Uploads')->find();

        foreach ($uploads as $upload) {
            _d(WWW_ROOT . Configure::read('App.filesBaseUrl') . $upload->path . '/' . $upload->filename);
        }
    }

    private function iterateUploads()
    {
        $uploads = $this->loadModel('Upload.Uploads')->find();

        foreach ($uploads as $upload) {
            if (!$this->existsUpload($upload)) {
                $this->deleteUpload($upload);
            }
        }
    }

    private function existsUpload($upload)
    {
        foreach ($this->tables as $table => $fields) {
            $or = [];
            $Table = $this->getTableLocator()->get($table);

            foreach ($fields as $field) {
                if ($Table->hasField($field)) {
                    $or[] = "$field LIKE '%\"id\":{$upload->id},%'";
                }
            }

            $exists = $Table->find()
                ->where([
                    'OR' => $or
                ])
                ->first();

            if ($exists) {
                return true;
            }
        }

        return false;
    }

    private function deleteUpload($upload)
    {
        $this->out('Borrando upload ' . $upload->id . ' ' . $upload->content_type . ' - ' . $upload->filename);

        foreach ($upload->full_paths as $path) {
            if (file_exists($path)) {
                $this->out('Borrando ' . $path);
                // unlink($path);
            }
        }

        // $this->Uploads->delete($upload);
    }

    private function getTables()
    {
        $tables = [];
        $plugins = Plugin::loaded();

        foreach ($plugins as $plugin) {
            $models = $this->getModels($plugin);

            foreach ($models as $model) {
                $table = $this->getTableLocator()->get($plugin . '.' . $model);

                if ($table->hasBehavior('UploadJsonable')) {
                    $fields = $table->uploadsFields();
                    $table_name = $table->getTable();

                    if (isset($tables[$table_name])) {
                        $tables[$table_name] = $tables[$table_name] + $fields;
                        $tables[$table_name] = array_unique($tables[$table_name]);
                    } else {
                        $tables[$table_name] = $fields;
                    }
                }
            }
        }

        $this->tables = $tables;
    }

    private function getModels($plugin)
    {
        $path = Plugin::path($plugin);
        $table_path = $path . 'src/Model/Table';

        $folder = new Folder($table_path);
        $tree = $folder->tree(null, false, 'file');
        $tree = array_map(function ($value) use ($table_path) {
            $value = str_replace($table_path . '/', '', $value);
            $value = str_replace('Table.php', '', $value);
            return $value;
        }, $tree);

        return $tree;
    }

}
