<div class="uploads-header">
  <form cf-search-upload class="upload-search">
    <input class="form-control" type="text" placeholder="Buscar" ng-model="uploadSearch">
    <button class="btn btn-white" type="submit"><i class="fa fa-search"></i> Buscar</button>
  </form>
  <div class="uploads-header-add" ng-show="data.uploadType == 'multiple'">
    <button cf-upload-add-multiple class="btn btn-primary">Añadir</button>
  </div>
</div>

<div class="uploads">
  <div ng-repeat="content in data.contents" ng-class="{ 'upload-doc': data.fileType == 'doc', 'selected': content.selected }">
    <img ng-if="data.fileType == 'image'" cf-push-upload="content" cf-upload-select="content" ng-src="/files/{{ content.path }}/thm_{{ content.filename }}">
    <div ng-if="data.fileType == 'doc'" cf-push-upload="content" cf-upload-select="content">
      <img style="margin-bottom: 10px" ng-if="content.extension == 'jpg' || content.extension == 'svg' || content.extension == 'png'" cf-push-upload="content" cf-upload-select="content" ng-src="/files/{{ content.path }}/{{ content.filename }}">
      <div>
        <i class="pl-upload-fileicon fa fa-file-o"></i> <span class="pl-upload-extension">{{ content.extension }}</span> <span class="pl-upload-filesize">{{ content.filesize | filesize }}</span>
      </div>
    </div>
    <div ng-if="data.fileType == 'image'" class="uploads-filename"><a class="btn btn-white" size="big" cf-modal="'/admin/upload/uploads/view/' + content.id" ng-model="content"><i class="fa fa-search-plus"></i></a> {{ content.filename }}</div>
    <div ng-if="data.fileType == 'doc'" cf-push-upload="content" cf-upload-select="content" class="uploads-filename">{{ content.filename }}</div>
  </div>
</div>

<cf-paginator-modal></cf-paginator-modal>