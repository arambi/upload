<?php

use Cake\Routing\Router;

Router::plugin( 'Upload', function($routes) {
  $routes->fallbacks( 'InflectedRoute');
});
