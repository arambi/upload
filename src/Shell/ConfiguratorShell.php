<?php
namespace Upload\Shell;

use Cake\Console\Shell;
use Cake\Core\Plugin;
use Cake\Filesystem\Folder;

/**
 * Realiza la instalación de la aplicación
 */
class ConfiguratorShell extends Shell
{

/**
 * Crea el fichero de configuración por defecto del pl2ugin
 * 
 * @return void
 */
  public function main()
  {
    $data = "<?php 

      \$config ['Upload'] = [
        'post' => [
          'type' => 'image',
          'template' => 'image',
          'thumbail' => 'med',
          'config' => [
            'thumbnailQuality' => 100,
            'thumbnailSizes' => [
                'thm' => [
                  'size' => '100w',
                  'name' => 'Miniatura',
                ],
                'hor' => [
                  'size' => '400x300',
                  'name' => 'Horizontal',
                ],
                'square' => [
                  'size' => '400x400',
                  'name' => 'Cuadrada'
                ],
                'big' => [
                  'size' => '800w',
                  'name' => 'Grande'
                ]
            ],
          ]
        ],
        'slide' => [
          'type' => 'image',
          'template' => 'image',
          'thumbail' => 'thm',
          'config' => [
            'thumbnailQuality' => 80,
            'thumbnailSizes' => [
                'thm' => [
                  'size' => '150w',
                  'name' => 'Miniatura',
                ],
                'big' => [
                  'size' => '1600w',
                  'name' => 'Grande'
                ]
            ],
          ]
        ],  
        'default' => [
          'type' => 'image',
          'template' => 'image',
          'thumbail' => 'med',
          'config' => [
            'thumbnailQuality' => 95,
            'thumbnailSizes' => [
                'thm' => [
                  'size' => '100w',
                  'name' => 'Miniatura',
                ],
                'big' => [
                  'size' => '1100w',
                  'name' => 'Grande'
                ]
            ],
          ]
        ],
        'gallery' => [
          'type' => 'image',
          'template' => 'image',
          'thumbail' => 'med',
          'config' => [
            'thumbnailQuality' => 100,
            'thumbnailSizes' => [
                'thm' => [
                  'size' => '100w',
                  'name' => 'Miniatura',
                ],
                'hor' => [
                  'size' => '400x300',
                  'name' => 'Horizontal',
                ],
                'square' => [
                  'size' => '400x400',
                  'name' => 'Cuadrada'
                ],
                'big' => [
                  'size' => '800w',
                  'name' => 'Grande'
                ]
            ],
          ]
        ],
        'doc' => [
          'type' => 'doc',
        ]
      ];";

    // Creación de ficheros de configuración
    $this->writefile( $data );
  }

  public function writefile( $data )
  {
    $file = fopen( "config/upload.php", "wb");
    fwrite( $file, $data);
    fclose( $file);
    $this->out( 'Configuración para el plugin Upload creada');
  }

}