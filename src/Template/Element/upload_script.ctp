<script type="text/javascript">

  $(function() {
     var scope = angular.element('[ng-controller=<?= $ng_controller ?>]').scope();
     scope.<?= $key ?> = <?= is_object( $upload) ? json_encode( (array)$upload->toArray()) : ($multiple ? '[]' : 'null') ?>;
     scope.$apply();

    scope.deletePhoto = function( id, index){
      for( i in scope.uploads){
        if( scope.uploads[i].id == id){
          for( z in scope.<?= $key ?>){
            if( scope.<?= $key ?>[z].id == scope.uploads[i].asset.id){
              scope.<?= $key ?>.splice( z, 1);
            }
          }
          scope.uploads.splice( i, 1);
        }
      }
      setTimeout(function(){
        scope.$apply();
      })
    }


    var uploader = new plupload.Uploader({
        runtimes : 'html5,flash,silverlight,html4',
        browse_button : '<?= $id ?>',
        url : "/upload/Uploads/upload.json?key=<?= $upload_key ?>",
         
        filters : {
            max_file_size : '10mb',
            mime_types: [
                {title : "Image files", extensions : "jpg,gif,png,jpeg"}
            ]
        },
        file_data_name: 'filename',
        init: {
          FilesAdded: function(up, files) {
            plupload.each(files, function(file) {
              uploader.start();
            });
          },
   
          UploadProgress: function(up, file) {
            var scope = angular.element('[ng-controller=<?= $ng_controller ?>]').scope();
            scope.uploads = up.files;
            scope.$apply();
          },
   
          Error: function(up, err) {
              document.getElementById('console').innerHTML += "\nError #" + err.code + ": " + err.message;
          },
          FileUploaded: function( up, file, result){
            var response = JSON.parse( result.response);
            var scope = angular.element('[ng-controller=<?= $ng_controller ?>]').scope();
            <?php if( $multiple): ?>
              scope.<?= $key ?>.push( response.upload);
            <?php else: ?>
              scope.<?= $key ?> = response.upload;
            <?php endif ?>
            for( i in up.files){
              if( up.files[i].id == file.id){
                up.files[i].asset = response.upload;
              }
            }
            scope.uploads = up.files;
            scope.$apply();
            $('#<?= $id ?>').show();
            $('#<?= $progress_id ?>').removeClass( 'upload-show');
          }
        }
    });
     
    uploader.init();

  });
</script>