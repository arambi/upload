<?php
namespace Upload\Test\TestCase\Model\Behavior;

use Upload\Model\Behavior\UploadJsonableBehavior;
use Cake\TestSuite\TestCase;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\Query;
use Cake\ORM\Table;

class ContentsTable extends Table 
{

	public function initialize(array $options) 
  {
		$this->addBehavior( 'Upload.UploadJsonable', [
			'fields' => [
				'photo'
			]
		]);

    $this->addBehavior( 'Cofree.Jsonable', [
      'fields' => [
        'photo'
      ]
    ]);
	}
}

/**
 * Upload\Model\Behavior\UploadJsonableBehavior Test Case
 */
class UploadJsonableBehaviorTest extends TestCase {

	public $fixtures = [
    'plugin.cofree.contents',
  ];

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->connection = ConnectionManager::get('test');
    $this->Contents = new ContentsTable([
      'alias' => 'Contents',
      'table' => 'contents',
      'connection' => $this->connection
    ]);	
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() 
  {
		unset($this->Contents);

		parent::tearDown();
	}

/**
 * Comprueba si ha añadido el behavior Cofree.Jsonable
 */
	public function testConfig()
	{
		$behaviors =  $this->Contents->behaviors()->loaded();
    $this->assertTrue( in_array( 'Jsonable', $behaviors));
	}

  public function testSave()
  {
    $data = [
      'title' => 'Hola mundo',
      'photo' => [
        [
          'id' => 1,
          'filename' => 'photo.jpg',
          'path' => '10/20/30',
          'original_filename' => 'photo.jpg',
          'mimetype' => 'image/jpeg',
        ],
        [
          '_id' => 1,
          'filename' => 'photo.jpg',
          'path' => '10/20/30',
          'original_filename' => 'photo.jpg',
          'mimetype' => 'image/jpeg',
        ]
      ]
    ];

    $content = $this->Contents->newEntity( $data);
    $this->Contents->save( $content);
    $content = $this->Contents->get( $content->id);
    $this->assertEquals( 1, count( $content->photo));
  }

  protected function _appendSufix( $filename, $sufix)
  {
    if( $sufix > 0)
    {
      $name = substr( $filename, 0, strrpos( $filename, '.'));
      $ext = substr( $filename, strrpos( $filename, '.'));
      $filename = $name . '-'. $sufix . $ext;
    }

    return $filename;
  }


  public function testAppendSufix()
  {
    $filename = $this->_appendSufix( 'hola.jpg', 1);
  }
}
