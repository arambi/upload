<script type="text/javascript">

  $(function() {
     var scope = angular.element('[ng-controller=<?= $ng_controller ?>]').scope();
     scope.<?= $key ?> = <?= is_object( $upload) ? json_encode( (array)$upload->toArray()) : ($multiple ? '[]' : 'null') ?>;
     scope.$apply();

    scope.deletePhoto = function( key){
      scope[key] = null;
      setTimeout(function(){
        scope.$apply();
      })
    }


    var uploader = new plupload.Uploader({
        runtimes : 'html5,flash,silverlight,html4',
        browse_button : '<?= $id ?>',
        url : "/upload/Uploads/upload.json?key=<?= $upload_key ?>",
         
        filters : {
            max_file_size : '10mb',
            mime_types: [
                {title : "Image files", extensions : "jpg,gif,png,jpeg"}
            ]
        },
        file_data_name: 'filename',
        init: {
          FilesAdded: function(up, files) {
              plupload.each(files, function(file) {
                uploader.start();
                  // document.getElementById('filelist').innerHTML += '<div id="' + file.id + '">' + file.name + ' (' + plupload.formatSize(file.size) + ') <b></b></div>';
              });
          },
   
          UploadProgress: function(up, file) {
              // document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = '<span>' + file.percent + "%</span>";
          },
   
          Error: function(up, err) {
              document.getElementById('console').innerHTML += "\nError #" + err.code + ": " + err.message;
          },
          FileUploaded: function( up, file, result){
            var response = JSON.parse( result.response);
            var scope = angular.element('[ng-controller=<?= $ng_controller ?>]').scope();
            // scope.<?= $key ?>.push( response.upload);
            scope.<?= $key ?> = response.upload;
            scope.$apply();
          }
        }
    });
     
    uploader.init();

  });
</script>