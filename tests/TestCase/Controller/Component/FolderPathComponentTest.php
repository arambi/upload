<?php
namespace Upload\Test\TestCase\Controller\Component;

use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;
use Upload\Controller\Component\FolderPathComponent;

/**
 * Upload\Controller\Component\FolderPathComponent Test Case
 */
class FolderPathComponentTest extends TestCase
{

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->FolderPath = new FolderPathComponent($registry);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->FolderPath);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
      $path = $this->FolderPath->getPath();
      $this->assertTrue( array_key_exists( 'path', $path));
      $this->assertTrue( array_key_exists( 'urlPath', $path));
      $this->assertTrue( array_key_exists( 'serverPath', $path));
      $this->assertTrue( is_dir( $path ['serverPath']));
    }
}
